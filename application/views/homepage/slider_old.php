<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<link rel="StyleSheet" href="<?php echo base_url(); ?>css/slider.css" type="text/css" />
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>

<ul id="slideshow">
<? 
foreach ($query as $row)
{
?>
		<li>
			<h3>TinySlideshow v1</h3>
			<span><?php echo base_url(); ?>/<?=$row-> large_image?></span>
			<p><?=$row-> name?> : <?=$row-> description?></p>
			<a href="#"><img src="<?php echo base_url(); ?>/<?=$row-> small_image?>" alt="Orange Fish" /></a>
		</li>
        
<? }?>
		
        
    </ul>
    
    <div id="wrapper">
		<div id="fullsize">
			<div id="imgprev" class="imgnav" title="Previous Image"></div>
			<div id="imglink"></div>
			<div id="imgnext" class="imgnav" title="Next Image"></div>
			<div id="image"></div>
			<div id="information">
				<h3></h3>
				<p></p>
			</div>
		</div>
        
        
      <div id="thumbnails">
			<div id="slideleft" title="Slide Left"></div>
			<div id="slidearea">
				<div id="slider"></div>
			</div>
			<div id="slideright" title="Slide Right"></div>
		</div>
	</div>
    
    
<script type="text/javascript" src="<?php echo base_url(); ?>/js/slider/compressed.js"></script>

<script type="text/javascript">
	$('slideshow').style.display='none';

	$('wrapper').style.display='block';
	var slideshow=new TINY.slideshow("slideshow");
	window.onload=function(){
		slideshow.auto=true;
		slideshow.speed=5;
		slideshow.link="linkhover";
		slideshow.info="information";
		slideshow.thumbs="slider";
		slideshow.left="slideleft";
		slideshow.right="slideright";
		slideshow.scrollSpeed=4;
		slideshow.spacing=5;
		slideshow.active="#fff";
		slideshow.init("slideshow","image","imgprev","imgnext","imglink");
	}
</script>    
        
</body>
</html>