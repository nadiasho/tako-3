<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Add new game</title>
<link href="../../css/main.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>/css/main.css" rel="stylesheet" type="text/css" />

</head>

<body>
<div>
  <h2>أضافة لعبة جديدة</h2>
  <p>&nbsp;</p>
</div>
<form id="upload_file" dir="rtl"
	action="<?php echo base_url(); ?>index.php/game_controller/doupload"
	method="post" enctype="multipart/form-data">
<fieldset>


<table>
	<tr>
		<td class="input_titles">أسم اللعبة </td>
		<td align="right" class="col2"><input type="text" name="gameName" id="gameName"
			class="textbox" /></td>
	</tr>
	
	<tr>
		<td class="input_titles"><label>تحميل اللعبة</label></td>
		<td align="right" class="col2"><input type="file" name="gameFile" id="gameFile"
			class="filebrowse" /></td>
	</tr>
	<tr>
	  <td class="input_titles">او اكتب الURL</td>
	  <td align="right" class="col2"><label for="url"></label>
	    <input name="url" type="text" id="url" value="http://www.gametako.com" size="40" /></td>
	  </tr>
    <tr>
		<td class="input_titles"><label>صورة صغيرة</label></td>
		<td align="right" class="col2"><input type="file" name="small_image" id="small_image"
			class="filebrowse" />
		100 X 100 px Max </td>
	</tr>
     <tr>
		<td class="input_titles">صورة كبيرة </td>
		<td align="right" class="col2"><input type="file" name="large_image" id="large_image"
			class="filebrowse" />
	    400 X 400 px Max </td>
	</tr>
    
	
	<tr>
		<td class="input_titles">العرض</td>
		<td align="right" class="col2"><input name="gameWidth" type="text"
			class="textbox" id="gameWidth" size="5" maxlength="5" /></td>
	</tr>
	<tr>
		<td class="input_titles"><label>الطول</label></td>
		<td align="right" class="col2"><input name="gameHeight" type="text"
			class="textbox" id="gameHeight" size="5" maxlength="5" /></td>
	</tr>
	<tr>
		<td class="input_titles">التصنيف</td>
		<td align="right" class="col2"> 
     <?  echo $dropdown; ?>

        </td>
	</tr>
	
    <tr>
		<td class="input_titles">نوع اللعبة</td>
		<td align="right" class="col2"> 
     <?  echo $types; ?>

        </td>
	</tr>
    
    
    <tr>
		<td class="input_titles"><label>شرح مختصر</label></td>
		<td align="right" class="col2" id="description"><label for="description"></label>
	    <textarea name="description" id="description" cols="45" rows="5"></textarea></td>
	</tr>
    
     <tr>
		<td class="input_titles">تعليمات العبة </td>
		<td align="right" class="col2" >
	    <textarea name="instructions" id="instructions" cols="45" rows="5"></textarea></td>
	</tr>
    
      <tr>
		<td class="input_titles"><label> شرح لمحركات البحث</label></td>
		<td align="right" class="col2" >
	    <textarea name="meta_desc" id="meta_desc" cols="45" rows="5"></textarea></td>
	</tr>
      <tr>
        <td class="input_titles"><label> Tags </label></td>
        <td align="right" class="col2" ><textarea name="tags" id="tags" cols="45" rows="3">-ألعاب -2012-جديدة   </textarea></td>
      </tr>
    
	<tr>
		<td class="col1"></td>
		<td class="col2">
		<button type="submit" class="button">تحميل </button>
		</td>
	</tr>
</table>
</fieldset>
</form>

</body>
</html>