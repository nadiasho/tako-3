<form id="upload_file"
	action="<?php echo base_url(); ?>index.php/game_controller/doupload"
	method="post" enctype="multipart/form-data">
<fieldset>

<table>
	<tr>
		<td class="col1"><label>Name:</label></td>
		<td class="col2"><input type="text" name="gameName" id="gameName"
			class="textbox" /></td>
	</tr>
	<tr>
		<td class="col1"><label>Leader Board Name:</label></td>
		<td class="col2"><input type="text" name="gameLeaderBoardName"
			id="gameLeaderBoardName" class="textbox" /></td>
	</tr>
	<tr>
		<td class="col1"><label>Game:</label></td>
		<td class="col2"><input type="file" name="gameFile" id="gameFile"
			class="filebrowse" /></td>
	</tr>
	<tr>
		<td class="col1"><label>Tako id:</label></td>
		<td class="col2"><select name="gameTakoId" id="gameTakoId"
			class="textbox"></select></td>
	</tr>
	<tr>
		<td class="col1"><label>Width:</label></td>
		<td class="col2"><input type="text" name="gameWidth" id="gameWidth"
			class="textbox" /></td>
	</tr>
	<tr>
		<td class="col1"><label>Height:</label></td>
		<td class="col2"><input type="text" name="gameHeight" id="gameHeight"
			class="textbox" /></td>
	</tr>
	<tr>
		<td class="col1"><label>Order By:</label></td>
		<td class="col2"><select name="orderBy" class="textbox" id="orderBy">
			<option value="ASC">ASC</option>
			<option value="DESC">DESC</option>
		</select></td>
	</tr>
	<tr>
		<td class="col1"><label>Label:</label></td>
		<td class="col2"><input type="text" name="label" id="label"
			class="textbox" /></td>
	</tr>
	<tr>
		<td class="col1"></td>
		<td class="col2">
		<button type="submit" class="button">Upload</button>
		</td>
	</tr>
</table>
</fieldset>
</form>