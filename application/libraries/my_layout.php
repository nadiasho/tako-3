<?php

class MY_Layout extends CI_Controller {

    public $header = 'header';
    public $footer = 'footer';

    public function content($views = '', $data = '')
    {
        // load header
        if ($this->header)
        {
            $this->load->view($this->header, $data);
            $data = '';
        }

        // load content, can be more than one views
        if (is_array($views))
        {
            foreach ($views as $view)
            {
                $this->load->view($view, $data);
                $data = '';
            }
        }
        else
        {
            $this->load->view($views, $data);
        }

        // load footer
        if ($this->footer)
        {
            $this->load->view($this->footer);
        }
    }
}
