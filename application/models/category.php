<?php


class Category extends CI_Model {

	

	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}


    function getall_cats(){
    
		$query = $this->db->get('cats');
		
		return $query->result();
  
  }

	
	/// get category details by name 
	
	function get_details($name)
	{
	 ///
	  	$query = $this->db->get_where('cats', array('name'=> $name));
		return $query->result();
	 ///
	
	}
	
	
		/// get category details by SEO name 
	
	function get_details_seo($name)
	{
	 ///
	  	$query = $this->db->get_where('cats', array('seo_url'=> $name));
		return $query->result();
	 ///
	
	}
	
  /// get the games of the categories 
  function get_games_cat($cat_id)
  {
	 ///
	  	$query = $this->db->get_where('games', array('category_id'=> $cat_id,'published'=>1));
		return $query->result();
	///
  }
  
  ///////////////////////////////////////////////////////
  
   /// get the games of the categories // only featured 
  function get_games_catf($cat_id)
  {
	 ///
	  	$query = $this->db->get_where('games', array('category_id'=> $cat_id,'published'=>1, 'featured'=>1));
		return $query->result();
	///
  }
  
  ///////////////////////////////////////////////////////
	
}
