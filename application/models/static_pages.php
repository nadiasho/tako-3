<?php


class Static_pages extends CI_Model {

	

	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}


    function getall_cats(){
    
		$query = $this->db->get('cats');
		
		return $query->result();
  
  }

	
	/// get category details by name 
	
	function get_details($name)
	{
	 ///
	  	$query = $this->db->get_where('static_pages', array('name'=> $name));
		return $query->result();
	 ///
	
	}
	
  
  ///Get the news 
  	function get_news_seo($name)
	{
	 ///
	  	$query = $this->db->get_where('news', array('seo_url'=> $name));
		return $query->result();
	 ///
	
	}
	
	
	
	 ///Get all news 
  	function get_news_all($limitn, $type)
	{
	 ///
	 $this->db->where('type', $type); 
	 $this->db->order_by("id", "desc");
	 $query = $this->db->get('news',$limitn);
	return $query->result();
	 ///
	
	}
  ///////////////////////////////////////////////////////
	
}
