<?php

class Api extends CI_Model {



	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}

	

	

	function get_all_users()
	{
		$query = $this->db->get('users');
		return $query->result();
	}

	function get_all_scores($tako_id)
	{
		$query = $this->db->get_where('highscores', array('game' => $tako_id));
		return $query;
	}

	

	function get_game_by_tako($tako_id)
	{
		return $this->db->get_where('games' , array('id' => mysql_escape_string(trim($tako_id))))->result();
	}

	function getMaxScore($user_id , $leaderboard)
	{
		$this->db->select_max('score');
		$result = $this->db->get_where('highscores' , array('user' => mysql_escape_string(trim($user_id)) , 'leaderboard' => mysql_escape_string(trim($leaderboard))))->result();
		return $result;
	}
	
	function getMaxSave($user_id , $leaderboard)
	{
		$this->db->select_max('data');
		$result = $this->db->get_where('highscores' , array('user' => mysql_escape_string(trim($user_id)) , 'leaderboard' => mysql_escape_string(trim($leaderboard))))->result();
		return $result;
	}

	function getMaxScoreByUserIdAndGameId ($user_id , $game_id)
	{
		return $this->db->get_where('highscores' , array('user' => mysql_escape_string(trim($user_id)) , 'game' => mysql_escape_string(trim($game_id))))->result();
	}
	
	function getSaveByUserIdAndGameId ($user_id , $game_id)
	{
		return $this->db->get_where('highscores' , array('user' => mysql_escape_string(trim($user_id)) , 'game' => mysql_escape_string(trim($game_id))))->result();
	}

	function updateHighScore($score , $user , $leaderboard)
	{
		$data = array('score' => $score);
		$this->db->where(array('user' => mysql_escape_string(trim($user)), 'leaderboard' => mysql_escape_string(trim($leaderboard))));
		$this->db->update('highscores', $data);
	}
	
	function updateSave($save , $user , $leaderboard)
	{
		$data = array('data' => $save);
		$this->db->where(array('user' => mysql_escape_string(trim($user)), 'leaderboard' => mysql_escape_string(trim($leaderboard))));
		$this->db->update('highscores', $data);
	}

	function addHighScore($get_game_id , $score, $user, $date,  $leaderboard)
	{// game, score, user, date, leaderboard
		$data = array(
   'game' => mysql_escape_string(trim($get_game_id)) ,
   'score' => mysql_escape_string(trim($score)) ,
   'user' => mysql_escape_string(trim($user)),
   'date' => mysql_escape_string(trim($date)),
	'leaderboard' => mysql_escape_string(trim($leaderboard))	
		);

		$this->db->insert('highscores', $data);
	}

	
}
