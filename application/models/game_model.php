<?php

/// Doupload is what should work ( 02- Jan )

class Game_model extends CI_Model {

	//var $width   = '700';
	//var $height = '700';


	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}


    function getall_cats(){
    
		$query = $this->db->get('cats');
		
		return $query->result();
  
  }

	function get_all_scores($tako_id)
	{
		$query = $this->db->get_where('highscores', array('tako_id =' => $tako_id));
		return $query;
	}

	
	
    // insert new game 
	function insert_game($game, $small_image, $large_image, $creator)
	
	{
		
		$this->instructions   = $_POST['instructions']; 
		$this->description = $_POST['description'];
		$this->gametype = $_POST['type'];
		$this->category_id = $_POST['category_id'];
		$this->tags = $_POST['tags'];

		$this->name = $_POST['gameName'];
		$this->height = $_POST['gameHeight'];
		$this->width = $_POST['gameWidth'];
		$this->meta_desc = $_POST['meta_desc'];
		
		///////////////////////////////////////////////
		$this->url = $game;

		
		// sent varibles from the controller 
		$this->creator = $creator;
		$this->game = $game;
		$this->small_image = $small_image;
		$this->large_image = $large_image;
         
		 		
		$this->date_added    = date('Y-M-D');

		$this->db->insert('games', $this);
				
		return $this->db->insert_id();
		
		
	}


 /// get the random games 
 
 function get_random_games($value)
 {
  $sql = "SELECT * from games where published='1' ORDER BY RAND() 
		LIMIT ".$value;
		
	
	$res = $this->db->query($sql)->result(); 
	
	return $res;
  
  }
	
 /////other games  (home page 50 games)
 function get_other_games()
 {
	$sql = "SELECT * from games where published='1' ORDER BY RAND() 
		LIMIT 18";
	$res = $this->db->query($sql)->result(); 
	return $res;
 }
 
 //////////////////////////////////////////////////////
 function get_random_games_1()
 {
  $sql = "SELECT * from games where published='1' ORDER BY RAND() 
		LIMIT 1";
	
	$res = $this->db->query($sql)->result(); 
	
	return $res;
  
  }
  /// get the new games 
  	function get_new_games($value)
	{
		$this->db->where('published', 1); 
		$this->db->where('featured', 1); 
		$this->db->order_by("id", "desc");
		$query = $this->db->get('games',$value);
		

		return $query->result();
	}
	
	
	//check if this user already rated this game 
	function check_rate($user_id , $game_id)
	{
 
	$query = $this->db->get_where('ratings' , array('user_id' => $user_id, 'game_id' => $game_id ))->result();


	return $query;
	
	
	}
	
	/// check list 
	//check if this user already added this game 
	function check_list($user_id , $game_id)
	{
 
	$query = $this->db->get_where('favorites' , array('user_id' => $user_id, 'game_id' => $game_id ))->result();


	return $query;
	
	
	}
	
	/// end check list 
	
	/// find number of like 
	
	function find_current_like($id)
	{
 
		$query = $this->db->get_where('games' , array('id' => $id  ))->result();
		
		$new_nlike=0;
		
		foreach ($query as $like_info)
		{
		$new_nlike=	$like_info->nlike;
		}
		$new_nlike=$new_nlike+1;
		
		////
		$this->nlike=$new_nlike;
		
		$this->db->update('games', $this, array('id' => $id));
		
		////
		return $new_nlike;
	}
	
/// find number of dislike 

///Add comments points 

function add_comments_points($user_id)
{
 
		$query = $this->db->get_where('users' , array('id' => $user_id  ))->result();
		
		$new_comments=0;
		
		foreach ($query as $info)
		{
		$new_comments=	$info->ncomments;
		}
		
		$new_comments=$new_comments+1;
		
		////
		$this->ncomments=$new_comments;
		
		$this->db->update('users', $this, array('id' => $user_id));
		
		////
		return $new_comments;
}
///////////////////////////
	
	function find_current_dislike($id)
	{
 
		$query = $this->db->get_where('games' , array('id' => $id  ))->result();
		
		$new_ndislike=0;
		
		foreach ($query as $like_info)
		{
		$new_ndislik=	$like_info->ndislike;
		}
		$new_ndislik=$new_ndislik+1;
		
		////
		$this->ndislike=$new_ndislik;
		
		$this->db->update('games', $this, array('id' => $id));
		
		////
		return $new_ndislik;
	}
	
	
	////
	/// find number of dislike 

	
	function find_dislike($id)
	{
 
		$query = $this->db->get_where('games' , array('id' => $id  ))->result();
		
		$new_ndislike=0;
		
		foreach ($query as $like_info)
		{
		$new_ndislik=	$like_info->ndislike;
		}
		
		
		return $new_ndislik;
	}
	
	/// find number of like 

	
	function find_like($id)
	{
 
		$query = $this->db->get_where('games' , array('id' => $id  ))->result();
		
		$new_nlike=0;
		
		foreach ($query as $like_info)
		{
		$new_nlik=	$like_info->nlike;
		}
		
		
		return $new_nlik;
	}	
	
	/// Add comments 
	function add_comment($id, $user_id, $ipa, $comment , $name, $username, $user_image )
	{
		// sent varibles 
		$data['game_id'] = $id;
		$data['user_id'] = $user_id;
		$data['comment'] = $comment;
		$data['username'] = $username;
		$data['user_image'] = $user_image;
		$data['ip'] = $ipa; 		
		$data['date_added']    = date('Y-M-D');

		$this->db->insert('comments', $data);
		
		
		// insert into actvities for the user 
		$activity['user_id'] = $user_id;
		$activity['game_id'] =$id;
		$activity['type'] ='comments';
		
		
		// the activity string 
		$str="لقد قام بأضافة تعليق على";
		$str.=" - ";
		//$str.=$name;
		
		$activity['activity'] =$str;
		$activity['date_added']    = date('Y-M-D');
		
		$this->db->insert('activity', $activity);
		
		
		return true;
	}
	/// Add the rating into databasne 
	function add_rate($id, $user_id, $ipa, $value, $name )
	{
	
	// sent varibles 
		$data['game_id'] = $id;
		$data['user_id'] = $user_id;
		$data['rating'] = $value;
		$data['ip'] = $ipa; 		
		$data['date_added']    = date('Y-M-D');
		
		// find points 
		$query_points = $this->db->get_where('points' , array('name' => 'rate' ),1)->result();
		
		$points=0;
	   foreach ($query_points as $query_points_res )
	   {
	   $points=$query_points_res->value;
	   }
	   $activity['points']=$points;
	  
		//////

		$this->db->insert('ratings', $data);
		
		// insert into actvities for the user 
		$activity['user_id'] = $user_id;
		
		// the activity string 
		$str=" لقد قام بالتصويت ";
		//$str.=" - ";
		//$str.=$name;
		
		$activity['activity'] =$str;
		$activity['game_id'] =$id;
		$activity['type'] ='rating';
		$activity['date_added']    = date('Y-M-D');
		
		$this->db->insert('activity', $activity);
		
		return true;
		
	}
	
	
	//// add to my list 
	
	function add_fave($id, $user_id, $name )
	{
		// sent varibles 
		$data['game_id'] = $id;
		$data['user_id'] = $user_id;
		$data['date_added']    = date('Y-M-D');

		$this->db->insert('favorites', $data);
		
		// insert into actvities for the user 
		$activity['user_id'] = $user_id;
		$activity['game_id'] =$id;
		$activity['type'] ='add to list';
		
		// the activity string 
		$str="لقد أضاف العبة التالية الى قائمته - ";
		//$str.=" - ";
		//$str.=$name;
		//$str.=" - ";
		//$str.=" الى قائمته المفضلة";
		
		$activity['activity'] =$str;
		$activity['date_added']    = date('Y-M-D');
		
		$this->db->insert('activity', $activity);
		
		return true;
		
	}
	
	
	
	///Add to my friend list 
	
	
		function add_fr($friend_id, $user_id, $name )
	{
		// sent varibles 
		$data['friend_id'] = $friend_id;
		$data['user_id'] = $user_id;
		
		$this->db->insert('friends', $data);
		
		// insert into actvities for the user 
		$activity['user_id'] = $user_id;
		$activity['game_id'] =0;
		$activity['type'] ='add to my friends';
		
		// the activity string 
		$str="قام باضافة  ";

		$str.=" - ";

		$str.=$name;

	    $str.=" - ";

		$str.="الى اصدقائه";
		
		$activity['activity'] =$str;
		
		$activity['date_added']    = date('Y-M-D');
		
		$this->db->insert('activity', $activity);
		
		return true;
		
	}
	
	
	////////
	  /// get the top games 
  	function get_top_games()
	{
		
		
		$this->db->order_by("rating", "desc");
		$this->db->where('published', 1);  
	    $this->db->where('featured', 1); 
		$query = $this->db->get('games',7);
		
		
		return $query->result();
	}
	
	
    /// get the recomanded games 
  	function get_rec_games($value)
	{
		
		$query =$this->db->get_where('games',array('featured'=>1,'published'=>1),$value);
		return $query->result();
	}
	
 
   /// get the comments on a game 
   function get_comments_games($game_id)
   {
		
		$query =$this->db->get_where('comments',array('approved'=>1, 'game_id'=>$game_id));
		
		$result = $query->result_array();
		
		// loop to find the user image 
        
		    foreach( $result as $key => $row )
            {
                
                // add the children to the result array
                $query = $this->db->get_where('users',array('id'=>$row['user_id']));
                $row['user'] = $query->result_array();
                $result[$key] = $row;
                
            }
            
            return $result;
   } 
   
   
  /// get the most views games 
  	function get_mostviews_games($value)
	{
	   $this->db->order_by("hits", "desc"); 
		$this->db->where('published', 1);  
		$this->db->where('featured', 1); 

		$query = $this->db->get('games',$value);
		return $query->result();
	}
	
  /// get the slider
  	function get_slider_games()
	{
		$query = $this->db->get('slider',7);
		return $query->result();
	}
	
	
	// get featured games for the slider 
	
	
    function get_f_games()
	{
		$f=1; 
		$query = $this->db->get_where('games' , array('featured' => $f,'published' => $f  ))->result();
		
		return $query;
	}
	
/////find the points to be added 
function find_points($name)
{

$query = $this->db->get_where('points' , array('name' => $name ),1)->result();
		
return $query;
}

///add points to the user 
function add_points( $points, $id)
{


$query = $this->db->get_where('users' , array('id' => $id  ))->result();
		
		//$oldpoints=0;
		
		foreach ($query as $info)
		{
		$oldpoints=	$info->points;
		}
		$newpoints=$oldpoints+$points;
		//echo $newpoints;
		////
		$users->points=$newpoints;
		
		$this->db->update('users', $users, array('id' => $id));
		return $newpoints;
		
}
////////////////////////////////////////////////////
// get this game data 
	
	/// Get game by name 
    function get_this_game($name)
	{

		$query = $this->db->get_where('games' , array('name' => $name  ))->result();
		
		return $query;
	}
	
	/// Get game by SEO url  
  function get_this_game_seo($name)
  {
	
		$query = $this->db->get_where('games' , array('seo_url' => $name  ))->result();
		
		return $query;
		
  }


/// Get game by SEO like  
    function get_this_gamel($name)
	{


        if ($name ="كنجز-فالي-2-(وادي-الملوك-٢)-–-صخر-kings-valley-2-–-msx")
		{
			$name="وادي-الملوك-٢";
		}
		
		
        if ($name ="-(jame3-jame3-3d)-3d-جمع-جمع")
		{
			$name="جمع-جمع";
		}		
		
		
		if ($name ="رامبو-–-صخر-rambo-–-msx-")
		{
			$name="رامبو";
		}
		
		
		if ($name ="مغامرة-البطريق-(بنجوينز-أدفينشر)-–-صخر-penguin's-adventure-–-msx")
		{
			$name="بنجوينز";
		}
		
		
		if ($name ="كنجز-فالي-(وادي-الملوك)-–-صخر-kings-valley-–-msx")
		{
			$name="كنجز-فالي";
		}
		
		
		if ($name ="كاسيل-(الأميرة)-–-صخر-castle-–-msx")
		{
			$name="كاسيل الأميرة ";
		}
		
		if ($name ="رود-فايتر-(مقاتل-الطريق)-–-صخر-road-fighter-–-msx")
		{
			$name="مقاتل-الطريق";
		}
		
		
		if ($name ="تشارلي-سيركس-(سيرك-تشارلي)-–-صخر-circus-charlie-–-msx")
		{
			$name="سيرك-تشارلي";
		}
		
		if ($name ="كاسيل-اكسيلينت-(الأميرة-٢)-–-صخر-castle-excellent-–-msx")
		{
			$name="كاسيل-اكسيلينت";
		}
		
		if ($name ="كنجز-فالي-2-(وادي-الملوك-٢)-–-صخر-kings-valley-2-–-msx")
		{
			$name="وادي-الملوك-٢";
		}
		
        $this->db->like('name', $name);
		$this->db->or_like('seo_url', $name);

		$query = $this->db->get('games')->result();
		
		return $query;
	}

/////////////////////////////////////////////////////




	function deleteGame($game_id)
	{
		$game_id = mysql_escape_string(trim($game_id));
		$tako_id = $this->db->get_where('games' , array('id' => $game_id ))->result();
		if(!$tako_id)
		return false;
		 
		$tako_id = $tako_id[0]->tako_id;

		$deleteGame = $this->db->delete('games', array('id' => $game_id));
		$deleteHighScore =  $this->db->delete('highscores', array('game' => $game_id));
		
		if($deleteGame &&  $deleteHighScore )
		return true;
		else return false;
		

	}
	
	
	
///////////////find search results
function getSearchResults ($function_name, $description=TRUE)
	{
				

		$function_name2= str_replace("أ","ا", $function_name);
		$function_name3= str_replace("ا","أ", $function_name);
		$function_name4= str_replace("ء","أ", $function_name);


	

		$this->db->like('name', $function_name);
		$this->db->or_like('name', $function_name2);
		$this->db->or_like('name', $function_name3);
		$this->db->or_like('name', $function_name4);

		$query = $this->db->get('games');
		
		if ($query->num_rows() > 0) {
			$output = '<ul>';
			foreach ($query->result() as $function_info) {
				if ($description) {
					$output .= '<li><strong>' . $function_info->name . '</strong><br />';
					$output .= $function_info->description . '</li>';
				} else {
					$output .= '<li>' . $function_info->name . '</li>';
				}
			}
			$output .= '</ul>';
			return $query->result();
		} else {
			return $query->result();
		}
		
	}
	
	
	
	
	
	
	
	
}
