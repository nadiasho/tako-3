<?php

/// Doupload is what should work ( 02- Jan )

class User_model extends CI_Model {

	

	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}


    function getall_cats(){
    
		$query = $this->db->get('cats');
		
		return $query->result();
  
  }

	function get_all_scores($tako_id)
	{
		$query = $this->db->get_where('highscores', array('tako_id =' => $tako_id));
		return $query;
	}

	
	
    // insert new game 
	function insert_game($game, $small_image, $large_image, $creator)
	
	{
		
		$this->instructions   = $_POST['instructions']; 
		$this->description = $_POST['description'];
		$this->description = $_POST['description'];
		$this->gametype = $_POST['type'];
		$this->category_id = $_POST['category_id'];
		$this->name = $_POST['gameName'];
		$this->height = $_POST['gameHeight'];
		$this->width = $_POST['gameWidth'];
		$this->meta_desc = $_POST['meta_desc'];
		
		// sent varibles from the controller 
		$this->creator = $creator;
		$this->game = $game;
		$this->small_image = $small_image;
		$this->large_image = $large_image;
         
		 		
		$this->date_added    = date('Y-M-D');

		$this->db->insert('games', $this);
				
		return $this->db->insert_id();
		
		
	}

	

	
  /// get the new games 
  	function get_new_games()
	{
		
		$this->db->order_by("id", "desc");
		$query = $this->db->get('games',7);
		

		return $query->result();
	}
	
	
	  /// get the top games 
  	function get_top_games()
	{
		
		$this->db->order_by("nlike", "desc"); 
		$query = $this->db->get('games',7);
		
		return $query->result();
	}
	
	
    /// get the recomanded games 
  	function get_rec_games()
	{
		
		$query =$this->db->get_where('games',array('reco'=>1),7);
		return $query->result();
	}
	
 
  /// get the most views games 
  	function get_mostviews_games()
	{
	
	   $this->db->order_by("hits", "desc"); 

		$query = $this->db->get('games',7);
		return $query->result();
	}
	
  /// get the slider
  	function get_slider_games()
	{
		
		$query = $this->db->get('slider',7);
		return $query->result();
	}
	
	
	// get featured games for the slider 
	
	
    function get_profile_user($username)
	{
		
				
		$this->db->select('*');
		
        $this->db->from('users');
		
        $this->db->where(array('username'=> $username));
		
        $this->db->or_where(array('email'=> $username)); 

        $query = $this->db->get();

		return $query->result();

		
	}
	

	function login($username , $password)
	{
		$password5=md5($password);
		$sql = "SELECT * FROM `users` WHERE (`username`='{$username}' OR `email`='{$username}' ) AND `active`=1 AND  `password`='{$password5}'";
	    $res = $this->db->query($sql)->result();
		return $res;
	}
	
	
	//// get the user Id from users table 
	
	function get_user_id($username)
	{
	  $query = $this->db->get_where('users', array('username' => $username));
			
		return $query->result();	
	}
	
	//// get the user ID from facebook users table 
	
	function get_user_id_facebook($facebookid)
	{
	
	 $query = $this->db->get_where('users_facebook', array('facebook_user_id' => $facebookid));
			
	 return $query->result();	
		
	}
	
	
	
	
	//// get the user ID from facebook users table 
	
	function get_user_id_facebook_login($user_id)
	{
	
	 $query = $this->db->get_where('users_facebook', array('user_id' => $user_id));
			
	 return $query->result();	
		
	}
	
	
		//// get the user data from the database 
	
	function get_user_data($user_id)
	{
	
	 $query = $this->db->get_where('users', array('id' => $user_id));
			
	 return $query->result();	
		
	}
	
	
	///find user list (fav games)
	
	function find_user_list($user_id)
	{
	$query = $this->db->get_where('favorites', array('user_id' => $user_id));
			
	$result = $query->result_array();
		
		// loop to find the game image 
        
		    foreach( $result as $key => $row )
            {
                
                // add the children to the result array
                $query = $this->db->get_where('games',array('id'=>$row['game_id']));
                $row['games'] = $query->result_array();
                $result[$key] = $row;
                
            }
            
            return $result;
	}
	
	
	  /// get the test users 
  	function get_test_users()
	{
		
		$this->db->order_by("id", "desc");
		$query = $this->db->get('test_emails',7);
		

		return $query->result();
	}
	/// find all users 
	  	function get_all_users()
	{
		
		$this->db->order_by("id", "desc");
		$query = $this->db->get('users');
		

		return $query->result();
	}
	
	 /// find the email to be sent 
  	function get_email_temp()
	{
		
$query = $this->db->get_where('mail_temp' , array('main' => '0'  ))->result();
		
		return $query;
		
	}
	
}
