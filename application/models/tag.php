<?php


class Tag extends CI_Model {

	

	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}



	
	/// get category details by name 
	
	function get_details($name)
	{
	 ///
	  	$query = $this->db->get_where('cats', array('name'=> $name));
		return $query->result();
	 ///
	
	}
	
  /// get the games of the tag 
  function get_games_cat($name)
  {
	 ///
	     $this->db->like('tags', $name);
		$query = $this->db->get('games');
		return $query->result();
	///
  }
  ///////////////////////////////////////////////////////
	
}
