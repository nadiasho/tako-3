<?php

class Api_model extends CI_Model {



	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}

	function getGameById($game_id)
	{
		$query = $this->db->get_where('games', array('id' => mysql_escape_string(trim($game_id))));
		
		return $query->result();
	}

	

	function get_all_users()
	{
		$query = $this->db->get('users');
		return $query->result();
	}

	function get_all_scores($tako_id)
	{
		$query = $this->db->get_where('highscores', array('game' => $tako_id));
		return $query;
	}

	

	function get_game_by_tako($tako_id)
	{
		return $this->db->get_where('games' , array('tako_id' => mysql_escape_string(trim($tako_id))))->result();
	}
	
	function get_game_by_name($gameName)
	{
		return $this->db->get_where('games', array('name' => mysql_escape_string(trim($gameName))))->result();
	}

	function getMaxScore($user_id , $gameName)
	{
		$this->db->select_max('highscores.score');
		$this->db->from('highscores');
		$this->db->join('games', 'highscores.game = games.id');
		$this->db->where('games.name' , mysql_escape_string(trim($gameName)));
		$this->db->where('highscores.user', $user_id);
		         
		//$this->db->select_max('score');
		//$result = $this->db->get_where('highscores' , array('user' => mysql_escape_string(trim($user_id)) , 'leaderboard' => mysql_escape_string(trim($leaderboard))))->result();
		return $this->db->get()->result();
	}
	
	function getMaxScoreAll($gameName)
	{
		$this->db->select_max('highscores.score');
		$this->db->from('highscores');
		$this->db->where('games.name' , mysql_escape_string(trim($gameName)));
		         
		//$this->db->select_max('score');
		//$result = $this->db->get_where('highscores' , array('user' => mysql_escape_string(trim($user_id)) , 'leaderboard' => mysql_escape_string(trim($leaderboard))))->result();
		return $this->db->get()->result();
	}
	
	function getMaxSave($user_id , $gameName)
	{
		$this->db->select_max('date');
		$this->db->from('highscores');
		$this->db->join('games', 'highscores.game = games.id');
		$this->db->where('games.name', mysql_escape_string(trim($gameName)));
		$this->db->where('highscores.user', mysql_escape_string(trim($user_id)));
		//$result = $this->db->get_where('highscores' , array('user' => mysql_escape_string(trim($user_id)) , 'leaderboard' => mysql_escape_string(trim($leaderboard))))->result();
		return $this->db->get()->result();
		//return $result;
	}

	function getMaxScoreByUserIdAndGameId ($user_id , $game_id)
	{
		return $this->db->get_where('highscores' , array('user' => mysql_escape_string(trim($user_id)) , 'game' => mysql_escape_string(trim($game_id))))->result();
	}
	
	function getSaveByUserIdAndGameId ($user_id , $game_id)
	{
		return $this->db->get_where('highscores' , array('user' => mysql_escape_string(trim($user_id)) , 'game' => mysql_escape_string(trim($game_id))))->result();
	}

	function updateHighScore($score , $user , $gameId)
	{
		$data = array('score' => mysql_escape_string(trim($score)));
			
		$this->db->where(array('user' => mysql_escape_string(trim($user)), 'game' => mysql_escape_string(trim($gameId))));
		$this->db->update('highscores', $data);
	}
	
	function addPointsForUser($user_id, $points)
	{
		$this->db->set('points', 'points +' . intval($points), false);
		$this->db->where('id', mysql_escape_string(trim($user_id)));
		$this->db->update('users');
	}
	
	function getPointsFor($name)
	{
		return $this->db->get_where('points', array('name' => mysql_escape_string(trim($name))))->result();
	}
	
	function updateSave($save , $user , $leaderboard)
	{
		$data = array('data' => $save);
		$this->db->where(array('user' => mysql_escape_string(trim($user)), 'leaderboard' => mysql_escape_string(trim($leaderboard))));
		$this->db->update('highscores', $data);
	}

	function addHighScore($get_game_id , $score, $user, $date)
	{// game, score, user, date, leaderboard
		$data = array(
   'game' => mysql_escape_string(trim($get_game_id)) ,
   'score' => mysql_escape_string(trim($score)) ,
   'user' => mysql_escape_string(trim($user)),
   'date' => mysql_escape_string(trim($date)),
	'leaderboard' => '' //mysql_escape_string(trim($leaderboard))	
		);

		$this->db->insert('highscores', $data);
	}
	
	function addHighScoreActivity($user_id, $game_id, $points)
	{
		$data = array(
		'user_id' => mysql_escape_string(trim($user_id)),
		'game_id' => mysql_escape_string(trim($game_id)),
		'type' => 'highscore',
		'points' => mysql_escape_string(trim($points)),
		'date_added' => date('Y-m-d H:i:s'),
		'activity' => 'حصل على أعلى علامة في لعبة'
		);
		
		$this->db->insert('activity', $data);
	}

	function addRecordToPlay($user_id, $game_id, $ipa)
	{
		$data = array(
		'user_id' => mysql_escape_string(trim($user_id)),
		'game_id' => mysql_escape_string(trim($game_id)),
		'ipa' => mysql_escape_string(trim($ipa)),
		'date_added' => date('Y-m-d H:i:s')
		);
		
		$this->db->insert('plays', $data);
	}
	
	function plusOneHitsForGame($game_id)
	{
		$this->db->set('hits', 'hits + 1', false);
		$this->db->where('id', mysql_escape_string(trim($game_id)));
		$this->db->update('games');
	}
	
	function addPlayActivity($user, $game_id, $points)
	{
		$data = array(
		'user_id' => mysql_escape_string(trim($user)),
		'game_id' => mysql_escape_string(trim($game_id)),
		'type' => 'play',
		'points' => mysql_escape_string(trim($points)),
		'activity' => 'قام بلعب لعبة',
		'date_added' => date('Y-m-d H:i:s')	
		);
		
		$this->db->insert('activity', $data);
	}
	
	function addFacebookShare($user_id,$game_id,$ipa)
	{
		$data = array(
		'user_id' => mysql_escape_string(trim($user_id)),
		'game_id' => mysql_escape_string(trim($game_id)),
		'ipa' => mysql_escape_string(trim($ipa)),
		'date_added' => date('Y-m-d H:i:s')
		);
		
		$this->db->insert('face_count',$data);
	}
	
	function addShareActivity($user_id, $game_id, $points)
	{
		$data = array(
		'user_id' => mysql_escape_string(trim($user_id)),
		'game_id' => mysql_escape_string(trim($game_id)),
		'type' => 'share',
		'points' => mysql_escape_string(trim($points)),
		'activity' => 'قام بنشر اللعبة على الفيسبوك',
		'date_added' => date('Y-m-d H:i:s')
		);
		
		$this->db->insert('activity', $data);
	}
}
