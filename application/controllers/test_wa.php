<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Small test suite for the Amazon Simple Email Service library
 *
 * @see /application/libraries/Amazon_ses.php
 */
class Test_wa extends CI_Controller {

	function index() 
	{
  
  	$this->load->model('user_model', "", true);
   
   // find the email to be sent 
   $emailtemp=$this->user_model->get_email_temp();
   
   foreach ($emailtemp as $temp)
   {
	   $inside=$temp->inside;
	    $title=$temp->title;
   }
   
   
  /// find users 
  $emails=$this->user_model->get_test_users();
  
  foreach ($emails as $emailsdata )
  { 
  ///
  $this->amazon_ses->to($emailsdata->email);
  $this->amazon_ses->from('admin@gametako.com', 'GameTako');
  $this->amazon_ses->subject($emailsdata->username." ".$title);
  echo "Sent to  ".$emailsdata->username."<br>";
  
  $this->amazon_ses->message($inside);
							   
  $this->amazon_ses->message_alt('No HTML?!');
  $this->amazon_ses->send();
  }

	}
	
	
	
	function all() 
	{
  
  	$this->load->model('user_model', "", true);
   
   // find the email to be sent 
   $emailtemp=$this->user_model->get_email_temp();
   
   foreach ($emailtemp as $temp)
   {
	   $inside=$temp->inside;
	    $title=$temp->title;
   }
   
   
  /// find users 
  $emails=$this->user_model->get_all_users();
  
  foreach ($emails as $emailsdata )
  { 
  ///
  $this->amazon_ses->to($emailsdata->email);
  $this->amazon_ses->from('admin@gametako.com', 'GameTako');
  $this->amazon_ses->subject($emailsdata->username." ".$title);
  echo "Sent to  ".$emailsdata->username."<br>";
  
  $this->amazon_ses->message($inside);
							   
  $this->amazon_ses->message_alt('No HTML?!');
  $this->amazon_ses->send();
  }

	}
}