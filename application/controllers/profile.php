<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {

    /**
     * Constructor - Access Codeigniter's controller object
     * 
     */
    function __construct() {
	parent::__construct();
	//Load the session library - If session lib is autoloaded remove this from here
	$this->load->library('session');
	//Load the user helper - If the helper is autoloaded remove this from here
	$this->load->helper('user');
	//Load the user model
	$this->load->model('user');
	$this->load->model('site_model');
 
		
		$this->load->model('user_model');
		
		
		$this->load->database();
		
		$this->load->helper('form');
		
		$this->load->helper('url');
    }
	

public function index()
{
       
	


	
	
}	


/////////// function to show user profile 

public function showprofile($username)
{
       
	// loading the data 
	 $user_id=0;
	 
	 // get the main cats for the header 
	$data['main_cat'] = $this->site_model->get_main_cats();
	
	///get the profile data 
	$data['user']=$this->user_model->get_profile_user($username);
	
	///this to find the profile id 
	$profile_data=$this->user_model->get_profile_user($username);
	
	$profile_id=0;
	foreach ($profile_data as $profile)
	{
		$profile_id=$profile->id;
		$username=$profile->username;
		$data['username']=$profile->username;
		$metades =$profile-> username;
		$metades.=" ";

		$metades.=$profile-> email;

		$data['metades']=$metades;

	}
    
	/// check if there are no profile with this name 
	if ($profile_id==0)
	{
		redirect('profile/wrong_name');
	}
	/////////////////////////////////////////////////////

	$username= $this->session->userdata('username');
   
    $login_type= $this->session->userdata('login_type');   
	
	$session_id = $this->session->userdata('username');
	   
	   // Get all sessions 
	$data["user_session"]=$this->session->all_userdata();
		
	//if the user not logged in 
		if (!$session_id)
		{
			$data["user_session"]['logged_in']="FALSE";
			$data["user_session"]['name']=0;
			$data["user_session"]['facebookid']=0;
			$data["user_session"]['login_type']="Not Logged";
			$data["user_session"]['username']="زائر";
			$data["user_id"]=0;
			$facebookid=0;
		}
		
		
   
	 $user_image="none";
	
	 $facebookid=$this->session->userdata('facebookid');
	 
	  if ($login_type=="Normal")
	  {
	   
	   $user_data=$this->user_model->get_user_id($username);
	   
	   foreach ($user_data as $userdata )
	   {
	   $user_id=$userdata->id;
	   $username=$userdata->username;
	   $user_image=$userdata->profile_image_url;
	   }
	 }

     //////if login through facebook 
	 
      if ($login_type=="FACEBOOK")
	  {
	   
	   $user_data_facebook=$this->user_model->get_user_id_facebook($facebookid);
	
	   foreach ($user_data_facebook as $userdata )
	   {
	   $user_id=$userdata->user_id;
	   $user_image=$userdata->profile_image_url;

	   }
	 }

     ///Set User ID and image 
	 $data["user_id"]=$user_id;
	 
	 // find the games for this user 
	 ///////will add code here 
	/////////////////////////////////////
	 
	 $data["user_image"]=$user_image;
	 
	 $data["logged_in"]=$data["user_session"]['logged_in'];
	
	//find the list of the games for this user 
	$data["user_list"]=$this->user_model->find_user_list($profile_id);

	///////////////////////////////////////////////////////
	//$data["user_id"]=$data["user_session"]['user_id'];
	  
	 $this->load->view("layouts/homepage", $data); 
	 $this->load->view('activity/activity', $data);
	 $this->load->view("layouts/header", $data );
	 $this->load->view("users/profile", $data );
	 $this->load->view("layouts/footer");


	
	
}	


public function addusers()
{

	$this->load->model('user');
	$user_id = $this->user->insert_user();

	
}


public function delete_frined($user_id, $friend_id, $username )
{

	$this->load->model('user');
	$user_id = $this->user->delete_friend($user_id, $friend_id);
	$r='/profile/';
	$r.=$username;
	redirect($r);

	
}


/// wrong profile 

public function wrong_name()
{
	
	// check if the user logged in 
		$session_id = $this->session->userdata('username');
		
		// Get all sessions 
		$data["user_session"]=$this->session->all_userdata();
		
		//if the user not logged in 
		if (!$session_id)
		{
			$data["user_session"]['logged_in']="FALSE";
			$data["user_session"]['name']=0;
			$data["user_session"]['facebookid']=0;
			$data["user_session"]['login_type']="Not Logged";
			$data["user_session"]['username']="زائر";
		}
		
	    ///
		$facebookid=$data["user_session"]['facebookid'];
		
       /// find the IP address 
	   $ipa=$_SERVER['REMOTE_ADDR'];
	   $data["user_session"]['ipa']=$ipa;
		
      //get the current user ID
	  $username=$this->session->userdata('username');
	  $login_type=$this->session->userdata('login_type');
	  $user_id=0;
	  $user_image="none";
	  
	  if ($login_type=="Normal")
	  {
	   
	   $user_data=$this->user_model->get_user_id($username);
	   
	   foreach ($user_data as $userdata )
	   {
	   $user_id=$userdata->id;
	   $user_image=$userdata->profile_image_url;
	   }
	 }

     //////if login through facebook 
	 
      if ($login_type=="FACEBOOK")
	  {
	   
	   $user_data_facebook=$this->user_model->get_user_id_facebook($facebookid);
	   
	   foreach ($user_data_facebook as $userdata )
	   {
	   $user_id=$userdata->user_id;
	   $user_image=$userdata->profile_image_url;
	   	   
	   }
	
	
	 }
	 
	 ///Set User ID and image 
	 $data["user_id"]=$user_id;
	 $data["user_image"]=$user_image;
	 
	 //Loding the view 
	  $data["wrong"]="هذا المستخدم غير موجود";
	  $this->load->view('layouts/homepage');
	  $this->load->view('layouts/header', $data);
      $this->load->view('wrong', $data);
	  $this->load->view('layouts/footer');
}	
} // end of the class 
	