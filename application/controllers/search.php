<?php
class Search extends CI_Controller {



  
   function __construct()
	{
 		parent::__construct();
		
		$this->load->library('form_validation');
		$this->load->database();
		$this->load->helper('form');
		$this->load->helper('user');
		$this->load->helper('url');
		
		//load models 
		$this->load->model('search', "", true);
		$this->load->model('site_model', "", true);
	    $this->load->model('category', "", true);
		$this->load->model('user_model', "", true);
		$this->load->model('adv', "", true);
	
	}

	public function index()
	{
		$this->load->model('search');

		$this->output->cache(120);
		$data['title'] = "نتائج البحث";
		$data['extraHeadContent'] = '<script type="text/javascript" src="' . base_url() . 'js/function_search.js"></script>';
		$this->load->view('search/body', $data);
	}


	public	function ajaxsearch()
	{
		$function_name = $this->input->post('function_name');
		$description = $this->input->post('description');
		echo $this->search->getSearchResults($function_name, $description);

	}

	public	function searchres()
	{
		$data['title'] = "Code Igniter Search Results";
		//$function_name = $this->input->post('function_name');
		//$data['search_results'] = $this->search->getSearchResults($function_name);
		echo "hi";
		//$this->load->view('search/body', $data);
	}

}
?>