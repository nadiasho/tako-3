<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//game controller  

class Game extends CI_Controller {

	 
	                
	function __construct()
	{
 		parent::__construct();
		// loading the helpers 
		$this->load->helper('form');
		$this->load->helper('user');
		$this->load->helper('url');
		
		//load models 
		$this->load->model('game_model', "", true);
		$this->load->model('site_model', "", true);
		$this->load->model('user_model', "", true);
		$this->load->model('adv', "", true);
		
	
	}
	
	public function index($seoname=TRUE)
	{
    // echo $seoname;
	 $name=$seoname;
	 //redirect("game_func/play/".$seoname);
	 
	 // check if the user logged in 
		$session_id = $this->session->userdata('username');
		
		// Get all sessions 
		$data["user_session"]=$this->session->all_userdata();
		
		//if the user not logged in 
		if (!$session_id)
		{
			$data["user_session"]['logged_in']="FALSE";
			$data["user_session"]['name']=0;
			$data["user_session"]['facebookid']=0;
			$data["user_session"]['login_type']="Not Logged";
			$data["user_session"]['username']="زائر";
		}
		
	    ///
		$facebookid=$data["user_session"]['facebookid'];
		
       /// find the IP address 
	   $ipa=$_SERVER['REMOTE_ADDR'];
	   $data["user_session"]['ipa']=$ipa;
		
      //get the current user ID
	  $username=$this->session->userdata('username');
	  $login_type=$this->session->userdata('login_type');
	  $user_id=0;
	  $user_image="none";
	  
	  if ($login_type=="Normal")
	  {
	   
	   $user_data=$this->user_model->get_user_id($username);
	   
	   foreach ($user_data as $userdata )
	   {
	   $user_id=$userdata->id;
	   $user_image=$userdata->profile_image_url;
	   }
	 }

     //////if login through facebook 
	 
      if ($login_type=="FACEBOOK")
	  {
	   
	   $user_data_facebook=$this->user_model->get_user_id_facebook($facebookid);
	   
	   foreach ($user_data_facebook as $userdata )
	   {
	   $user_id=$userdata->user_id;
	   $user_image=$userdata->profile_image_url;
	   	   
	   }
	
	
	 }
	 
	 ///Set User ID and image 
	 $data["user_id"]=$user_id;
	 $data["user_image"]=$user_image;
	 
	 
	 
	/// get this game data 
	
	/// Get by SEO URL 	
	$data['game']= $this->game_model->get_this_game_seo($name);

	$game_id=0;
	//get game id 
      foreach ($data['game'] as $gameid)
		{
			$game_id=$gameid->id;
			// finding the meta descr
		   //$metades=$name." لعبة جميلة العبها" .$gameid->tags; 
		   $metades = strip_tags($gameid-> description);
	
	      $metades = mb_substr($metades, 0, 255,'UTF8');
		  
		   $data['metades']=$metades;
		   $data['metatitle']=$gameid->name;


		}
	
	// if no game selected 
	if ($game_id==0)
	{
		//Get by name
		$data['game']= $this->game_model->get_this_game($name);
		foreach ($data['game'] as $gameid)
		{
			$game_id=$gameid->id;
			// finding the meta descr
		   //$metades=$name." لعبة جميلة العبها" .$gameid->tags; 
		   
		   $metades = strip_tags($gameid-> description);
	       $metades = mb_substr($metades, 0, 255,'UTF8');
		  
		 
		   $data['metades']=$metades;
		   $data['metatitle']=$gameid->name;


		}
		
		
         		// if no game selected 
	if ($game_id==0)
	{
		//Get by name
		$data['game']= $this->game_model->get_this_gamel($name);
		foreach ($data['game'] as $gameid)
		{
			$game_id=$gameid->id;
			// finding the meta descr
		   //$metades=$name." لعبة جميلة العبها" .$gameid->tags; 
		     $metades = strip_tags($gameid-> description);
	
	      $metades = mb_substr($metades, 0, 255,'UTF8');
		  
		   $data['metades']=$metades;
		   $data['metatitle']=$gameid->name;


		}
	}
	
	
        if ($game_id==0){	redirect("/game_func/wrong_name");}
	}	
	
	/// get the comments 
	$data["comments_data"]=$this->game_model->get_comments_games($game_id);
	
	
	/// get random games 
	    
	 $data["randgames"]=$this->game_model->get_random_games(7);	
	 $data["randgames2"]=$this->game_model->get_random_games(7);
		
	//check if the user can rate this game 
	$can_rate=1;
	
	$check_rate=$this->game_model->check_rate($user_id , $game_id);
	
	foreach ($check_rate as $yescan)
	{
		$can_rate=0;
	}
	
	$data["can_rate"]=$can_rate;
	
	
	
	///check if the user can add this game to his list 
	
	$can_add=1;
	
	$check_list=$this->game_model->check_list($user_id , $game_id);
	
	foreach ($check_list as $yescanadd)
	{
		$can_add=0;
	}
	
	$data["can_add"]=$can_add;
		
	//get the leader adv	
	$data['adv']= $this->adv->get_this_game_adv($game_id);
	
	//Loding the view 
		
	  $this->load->view('layouts/homepage', $data);
	  $data['main_cat'] = $this->site_model->get_main_cats();
	  $this->load->view('activity/activity', $data);
	  $this->load->view('layouts/header', $data);
      $this->load->view('game/view_game', $data);
	
	  $this->load->view('layouts/footer');

		
	}

		

 
 


}

//end of the functions 
