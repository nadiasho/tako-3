<? class Register extends CI_Controller {

    /**
     * Constructor - Access Codeigniter's controller object
     * 
     */
    function __construct() {
	parent::__construct();
	//Load the session library - If session lib is autoloaded remove this from here
	$this->load->library('session');
	//Load the user helper - If the helper is autoloaded remove this from here
	$this->load->helper('user');
	//Load the user model
	$this->load->model('user');
	$this->load->model('site_model');
    //$this->load->library('MY_Layout');
    }
	

public function index()
{
        $this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
        $this->form_validation->set_rules('username', 'أسم المستخدم', 'callback_username_check|is_unique[users.username]');
		$this->form_validation->set_rules('password', 'كلمة السر', 'trim|required|md5');
		$this->form_validation->set_rules('email', 'البريد الالكتروني', 'trim|required|valid_email|is_unique[users.email]');
		$this->form_validation->set_message('required', '  لقد نسيت ادخال');
		$this->form_validation->set_message('callback_username_check', 'غير متوفر اسم المستخدم');
		$this->form_validation->set_message('valid_email', 'ليس صحيحا البريد الالكتروني');


         /// if the data not vaild     
		if ($this->form_validation->run() == FALSE)
		{
		
		// loading the data 
	      $data['main_cat'] = $this->site_model->get_main_cats();
	
	  $this->load->view("layouts/homepage");
	  $this->load->view("layouts/regheader", $data );
	   $this->load->view("register/index");
	  $this->load->view("layouts/footer");

		}
		else
		{
		    $username = $this->input->post('username');
			$password = $this->input->post('password');
			$email = $this->input->post('email');

           //adding data to the users table 
		   
		   $user_id = $this->user->insert_user($username, $password, $email);
		   
		   if (isset($user_id))
		   {
			 $data["user_id"]=$user_id;
			 $data["username"]=$username;
			  
			  $this->load->view('layouts/homepage',$data);
			  $this->load->view('layouts/regheader',$data);
			  
			  $this->load->view('users/thankyou',$data);
			  $this->load->view('layouts/footer',$data);
			   
			}

		}
	
	
}	
	


public function addusers()
{

	$this->load->model('user');
	$user_id = $this->user->insert_user();
}

/// activate the user after clicking the email link 	
public function activate($token)

{
	$this->load->model('user');
	$active=$this->user->activate($token);
		
	$this->load->view('users/activated');

}	
	
} // end of the class 
	