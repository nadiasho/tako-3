<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Game_controller extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->model('game_model', "", true);

		
	}

	

	public function addgame()
	{
	 		$this->load->view('admin/add_game');

	}

	public function get_all_scores()
	{
		$query = $this->game_model->get_all_scores($_GET['tako_id']);
		echo $query;
	}

	public function logout()
	{
		unset($_SESSION);
		session_destroy();
		$this->load->view('game_view');
		header("Location: " . base_url());

	}
	
	public function doupload()
	{
		$status = "";
		$msg = "";
		
		//game name 
		
		$file_element_name = 'gameFile';
		// small image
		 
		$small_image = 'small_image';

		//large image 
		$large_image = 'large_image';
		
		//adding the url 
	
		
		$url=$_POST['url'];
		//echo $url;

		
		if (isset ($_session['user']) )
		{ 
		$id=$_SESSION['user'][0]->id;
		}
		else 
		$id=1; // admin
		
	

		if ($status != "error")
		{
			$config['upload_path'] = './files/';
			$config['allowed_types'] = 'gif|jpg|png|doc|txt|swf|unity|unity3d';
			$config['max_size']  = 1000000000;
			$config['encrypt_name'] = TRUE;

			$this->load->library('upload', $config);
			
			$this->upload->do_upload($small_image);
			$image1 = $this->upload->data();
			
			$this->upload->do_upload($large_image);
			$image2 = $this->upload->data();
           
			if (!$this->upload->do_upload($file_element_name)&&($url=='http://www.gametako.com'))
			
			{
				$status = 'error';
				$msg = $this->upload->display_errors('', '');
			}
			
			else
			
			{
				if(!isset($this->game_model)) $this->load->model('game_model', "", true);

				$data = $this->upload->data();
				
				
				
			   $dataType = 'int';
				if ($url !='http://www.gametako.com' )
				{
					$fileloc=$url;
				}else 
				if ($url =='http://www.gametako.com' )
				{
					$fileloc="http://www.gametako.com/files/" . $data['file_name'];
				}
	        
			  $game_id = $this->game_model->insert_game($fileloc,"http://www.gametako.com/files/" . $image1['file_name'],"http://www.gametako.com/files/" . $image2['file_name'],$id );
				
					
				
				if($game_id)
				{
					$date['status'] = "success";
					$date['msg'] = "Game added successfully ";
					
				}
				else
				{
					unlink($data['full_path']);
					$date['status'] = "faild";
					$date['msg'] = "File not uploaded";
				}
				
				$this->load->view('games/uploaded', $date);
			}
			@unlink($_FILES[$file_element_name]);
		}

	}

	public function mygamesFeed()
	{
		if(!isset($this->user_model)) $this->load->model('user_model', "", true);

		$myGames = $this->user_model->get_my_games($_SESSION['user'][0]->id);

		echo json_encode(array('games' => $myGames));
	}

	public function getMaxscore()
	{
		if(!isset($this->user_model)) $this->load->model('user_model', "", true);
		if(!isset($_POST['user_id']) || !isset($_POST['game_id']))
		echo json_encode(array('result' => 'error' , 'msg' => 'Either user or game was not passed correctly!'));

		$score = $this->user_model->getMaxScoreByUserIdAndGameId($_POST['user_id'] , $_POST['game_id']);
		echo json_encode(array('result' => 'success' , 'score' => $score));
	}

	public function getMaxScoreInsideGame()
	{
		if(!isset($this->user_model)) $this->load->model('user_model', "", true);
		if(!isset($_POST['userID']) || !isset($_POST['boardID']))
		echo json_encode(array('result' => 'error' , 'msg' => 'Either user or tako id was not passed correctly!'));
		$score = $this->user_model->getMaxScore($_POST['userID'] , $_POST['boardID']);
		$score = ($score && $score[0]->score)? $score[0]->score : -1;
		echo json_encode(intval($score));
	}
	
	public function getSaveInsideGame()
	{
		if(!isset($this->user_model)) $this->load->model('user_model', "", true);
		if(!isset($_POST['userID']) || !isset($_POST['boardID']))
		echo json_encode(array('result' => 'error' , 'msg' => 'Either user or tako id was not passed correctly!'));
		$score = $this->user_model->getMaxSave($_POST['userID'] , $_POST['boardID']);
		$score = ($score && $score[0]->data)? $score[0]->data : -1;
		echo $score;
	}

	public function addTakoHighscore()
		{
			if(!isset($this->user_model)) $this->load->model('user_model', "", true);
	
			$salt = "3ammom7md"; //our test salt
	
			//standard bridge parameters
			$user = $_POST['userID'];
			$username = $_POST['username'];
			$leaderboard = $_POST['boardID'];
			$score = $_POST['score'];
			$get_game = $this->user_model->get_game_by_tako($leaderboard); // mysql_fetch_array(mysql_query("SELECT * FROM ava_games WHERE tako_id = '$gametag'"));
			$date = date("Y-m-d H:i:s");
			$hashed = $_POST['hashed'];
	
			$hashphrase = $salt.$score.$user;
	
			//echo $hashed." not equile to ".$hashphrase;
	
			$myhash = sha1($hashphrase);
	
	
			if($hashed == $myhash)
			{
	
				$dbscore = $this->user_model->getMaxScore($user , $leaderboard);
				
				$dbscore = ($dbscore && $dbscore[0]->score)? $dbscore[0]->score : 0;
				
				if($dbscore){
					if ( $dbscore < $score){
						$this->user_model->updateHighScore($score , $user , $leaderboard);
						//mysql_query("UPDATE ava_highscores  SET score = $score WHERE user = '$user' AND leaderboard = '$leaderboard'") or die (mysql_error);
					}
				}
				else{
					$this->user_model->addHighScore($get_game[0]->id , $score, $user, $date,  $leaderboard);
					//	mysql_query("INSERT INTO ava_highscores (game, score, user, date, leaderboard) VALUES ($get_game[0]->id, $score, $user, '$date', '$leaderboard')") or die (mysql_error);
	
				}
			}
			else
			{
				echo $hashed." not equile to ".$myhash;
			}
		}
	
	public function addTakoSave()
		{
				if(!isset($this->user_model)) $this->load->model('user_model', "", true);
			
					$salt = "3ammom7md"; //our test salt
			
					//standard bridge parameters
					$user = $_POST['userID'];
					$username = $_POST['username'];
					$leaderboard = $_POST['boardID'];
					$save = $_POST['data'];
					$get_game = $this->user_model->get_game_by_tako($leaderboard); // mysql_fetch_array(mysql_query("SELECT * FROM ava_games WHERE tako_id = '$gametag'"));
					$date = date("Y-m-d H:i:s");
					$hashed = $_POST['hashed'];
			
					$hashphrase = $salt.$save.$user;
			
					//echo $hashed." not equile to ".$hashphrase;
			
					$myhash = sha1($hashphrase);
			
			
					if($hashed == $myhash)
					{
			
						$dbdata = $this->user_model->getScore($user , $leaderboard);
						
						$dbdata = ($dbdata && $dbdata[0]->save)? $dbscore[0]->save : 0;
						
						if($dbscore){
						
								$this->user_model->updateSave($save , $user , $leaderboard);
								//mysql_query("UPDATE ava_highscores  SET score = $score WHERE user = '$user' AND leaderboard = '$leaderboard'") or die (mysql_error);
						}
						else{
							$this->user_model->addSave($get_game[0]->id , $save, $user, $date,  $leaderboard);
							//	mysql_query("INSERT INTO ava_highscores (game, score, user, date, leaderboard) VALUES ($get_game[0]->id, $score, $user, '$date', '$leaderboard')") or die (mysql_error);
			
						}
			}
					else
					{
						echo $hashed." not equile to ".$myhash;
					}
	}

	public function getData()
	{
		if(!isset($this->user_model)) $this->load->model('user_model', "", true);
		if(!isset($_POST['user_id']) || !isset($_POST['game_id']))
		echo json_encode(array('result' => 'error' , 'msg' => 'Either user or game was not passed correctly!'));
		else
		{
			$score = $this->user_model->getMaxScoreByUserIdAndGameId($_POST['user_id'] , $_POST['game_id']);
			echo json_encode(array('result' => 'success' , 'score' => $score));
		}
	}

	public function deleteGame()
	{
		if(!isset($this->user_model)) $this->load->model('user_model', "", true);
		if(!isset($_POST['game_id']))
		echo json_encode(array('result' => 'error' , 'error' => 'Game was not passed correctly!'));
		else
		{
			$delete = $this->user_model->deleteGame($_POST['game_id']);
			if($delete)
			echo json_encode(array('result' => 'success'));
			else
			echo json_encode(array('result' => 'error' , 'error' => 'Unable To Delete Game!'));
		}




	}

	public function updatePassword()
	{
		if(!isset($this->user_model)) $this->load->model('user_model', "", true);
		if(!isset($_POST['oldPassword']))
		{
			echo json_encode(array('result' => 'error', 'error' => 'Old Password was not sent correctly!'));
			return;
		}

		if(!isset($_POST['newPassword']))
		{
			echo json_encode(array('result' => 'error' , 'error' => 'New Password was not sent correctly!'));
			return;
		}

		if(!isset($_POST['confirmPassword']))
		{
			echo json_encode(array('result' => 'error' , 'error' => 'Confirm Password was not sent correctly!'));
			return;
		}

		if($_POST['newPassword'] !== $_POST['confirmPassword'])
		{
			echo json_encode(array('result' => 'error', 'error' => 'New Password does not match with Confirm Password'));
			return;
		}

		if(strlen($_POST['newPassword']) < 4)
		{
			echo json_encode(array('result' => 'error' , 'error' => 'New Password length must be at least 4 characters'));
			return;
		}

		$newPassword = $this->user_model->updatePassword($_POST['oldPassword'], $_POST['newPassword'], $_SESSION['user'][0]->id);

		if(!$newPassword)
		{
			echo json_encode(array('result' => 'error' , 'error' => 'Un-Expected Error! Please Try again later'));
			return;
		}
		else if(!isset($newPassword['error']))
		echo json_encode(array('result' => 'success'));
		else
		 echo json_encode(array('result' => 'error' , 'error' => $newPassword['error']));
	}

        function mygamestakoid() {
        $result = array('success' => false, 'data' => null, 'error' => array());
        if(!isset($this->user_model)) $this->load->model('user_model', "", true);
        if(!isset($_SESSION['user'])) {$result['error'][] = 'user is not logged in'; echo json_encode($result); return;}

        $takos = $this->user_model->getTakosByDevId($_SESSION['user'][0]->id);
        
        if($takos)
        {
          $data = array();
          foreach($takos as $tako) 
          { $data[]['value']  = $tako->tako_id; }
          $result['success'] = true;
          $result['data'] = $data;
        }
        echo json_encode($result);



        }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
