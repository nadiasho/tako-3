<?
class Tag extends CI_Controller {

	 
	                
	function __construct()
	{
 		parent::__construct();
		
		$this->load->library('form_validation');
		$this->load->database();
		$this->load->helper('form');
		$this->load->helper('user');
		$this->load->helper('url');
		
		//load models 
		$this->load->model('game_model', "", true);
		$this->load->model('site_model', "", true);
	    $this->load->model('tag_model', "", true);
		$this->load->model('user_model', "", true);
		$this->load->model('adv', "", true);
	
	}
	
	public function index()
	{
	//
	
	// 
	}

	/// browse the main category page 
		
    public function browse($name)
    {
		
		
		///////Find the games for this tag /////////////////////////////
		/////////////////////////////////////////////////////////////////////
		
	   
	   
	     $cat_games=$this->tag_model->get_games_cat($name);
		$data["cat_games"]=$cat_games;
		///find the number of games 
		$numberofgames = 0; 
		foreach ($cat_games as $number)
		{
			$numberofgames=$numberofgames+1;
	   }
		$data['numberofgames']=$numberofgames;
	
		// cat name 
		$data['cat_name']=$name;
		//description				
		$data['description']='';
		//metatags				
		$data['metades']=" تصفح جيم تاكو ".$name;
		
		
		/////////////////////////////////////////////////////////////////////
		//Find the users data 
		/////////////////////////////////////////////////////////////////////

		 $username= $this->session->userdata('username');				////
   		 $login_type= $this->session->userdata('login_type');   		////
		 $facebookid=$this->session->userdata('facebookid');   	    ////
	   
	    // Get all sessions 
	    $data["user_session"]=$this->session->all_userdata();		   ////
		
    	//if the user not logged in 
		if (!$username)
		{
			$data["user_session"]['logged_in']="FALSE";
			$data["user_session"]['name']=0;
			$data["user_session"]['facebookid']=0;
			$data["user_session"]['login_type']="Not Logged";
			$data["user_session"]['username']="زائر";
			$data["user_id"]=0;
			$facebookid=0;
		}
		
	     $user_image="none";
		 $user_id=0;
	
	     if ($login_type=="Normal")
	     {
	   
	     $user_data=$this->user_model->get_user_id($username);
	      foreach ($user_data as $userdata )
	      {
	       $user_id=$userdata->id;
	       $user_image=$userdata->profile_image_url;
	       }
		   
	     }

     //////if login through facebook 
	 
      if ($login_type=="FACEBOOK")
	  {
	   $user_data_facebook=$this->user_model->get_user_id_facebook($facebookid);
	   foreach ($user_data_facebook as $userdata )
	   {
	   $user_id=$userdata->user_id;
	   $user_image=$userdata->profile_image_url;

	   }
	 }

   		  ///Set User ID and image 
	 	$data["user_id"]=$user_id;
		$data["user_image"]=$user_image;
		$data["logged_in"]=$data["user_session"]['logged_in'];
	
		/////////////////////////////////////////////////////////////////////
		/////End of user data ///////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////
	 	
		/////get the main cats for the header 
	    $data['main_cat'] = $this->site_model->get_main_cats();
		$data['tag_title']='العاب'; 
		$data['tag_title'].=' ';
		$data['tag_title'].=$name; 
 

	    
		
		
		///////////////////Load Views 
			 $this->load->view("layouts/homepage", $data );

	  $this->load->view("layouts/header", $data );
	    ///Load cat view 
		$this->load->view("cats/main", $data);
	    $this->load->view("layouts/footer");
		
	} // end of browse function 
 
 
 
 
 
 
 
 
 public function wrong_name()
 { 
 
 echo "لقد قمت بدخول تصنيف خاطئ";
 }


}// End of Cat Controller 
