<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends CI_Controller {

	public function index()
	{
		
	}

	public function get_all_scores()
	{
		$query = $this->api->get_all_scores($_GET['tako_id']);
		echo $query;
	}

protected function getIP() 
	{
		$ip;
		if (getenv("HTTP_CLIENT_IP"))
		$ip = getenv("HTTP_CLIENT_IP");
		else if(getenv("HTTP_X_FORWARDED_FOR"))
		$ip = getenv("HTTP_X_FORWARDED_FOR");
		else if(getenv("REMOTE_ADDR"))
		$ip = getenv("REMOTE_ADDR");
		else
		$ip = "UNKNOWN";
		return $ip;
	}

	
	public function viewgame()
	{
		if($this->session->userdata('logged_in'))
		{
			$this->load->model('api_model', "", true);
			
			$game_id = $_GET['game_id'];
			
			$game = $this->api_model->getGameById($game_id);
			
			if(count($game))
			{
				$this->load->view('api_view', array('game' => $game[0]));
			}
			else
			{
				die('اللعبة غير موجودة أو محذوفة!');
			}
			
		}
		else header('Location: /');
		

	}


	public function getMaxscore()
	{
		if(!isset($this->user_model)) $this->load->model('user_model', "", true);
		if(!isset($_POST['user_id']) || !isset($_POST['game_id']))
		echo json_encode(array('result' => 'error' , 'msg' => 'Either user or game was not passed correctly!'));

		$score = $this->user_model->getMaxScoreByUserIdAndGameId($_POST['user_id'] , $_POST['game_id']);
		echo json_encode(array('result' => 'success' , 'score' => $score));
	}

	public function getMaxScoreInsideGame()
	{
		if(!isset($this->api_model)) $this->load->model('api_model', "", true);
		if(!isset($_POST['userID']) || !isset($_POST['currentGameName']))
		echo json_encode(array('result' => 'error' , 'msg' => 'Either user or game id was not passed correctly!'));
		$score = $this->api_model->getMaxScore($_POST['userID'] , $_POST['currentGameName']);
			
		$score = ($score && $score[0]->score)? $score[0]->score : 0;
		echo json_encode(intval($score));
	}

	public function getSaveInsideGame()
	{
		if(!isset($this->api_model)) $this->load->model('api_model', "", true);
		if(!isset($_POST['userID']) || !isset($_POST['currentGameName']))
		echo json_encode(array('result' => 'error' , 'msg' => 'Either user or game id was not passed correctly!'));
		$score = $this->api_model->getMaxSave($_POST['userID'] , $_POST['currentGameName']);
		$score = ($score && $score[0]->date)? $score[0]->date : -1;
		echo json_encode($score);
	}

	public function addTakoHighscore()
	{
		if(!isset($this->api_model)) $this->load->model('api_model', "", true);

		$salt = "3ammom7md"; //our test salt

		//standard bridge parameters
		$user = $_POST['userID'];
		$username = $_POST['username'];
		$gameName = $_POST['currentGameName'];
		$score = $_POST['score'];
		$get_game = $this->api_model->get_game_by_name($gameName); // mysql_fetch_array(mysql_query("SELECT * FROM ava_games WHERE tako_id = '$gametag'"));
		$date = date("Y-m-d H:i:s");
		$hashed = $_POST['hashed'];
		$hashphrase = $salt.$score.$user;

		//echo $hashed." not equile to ".$hashphrase;

		$myhash = sha1($hashphrase);


		if($hashed == $myhash)
		{

			$dbscore = $this->api_model->getMaxScore($user , $gameName);

			$dbscore = ($dbscore && $dbscore[0]->score)? $dbscore[0]->score : 0;

			if($dbscore){
				if ( $dbscore < $score){
					$this->api_model->updateHighScore($score , $user , $get_game[0]->id);
					$pointsForHighScore = $this->api_model->getPointsFor('highscore');
						
					$this->api_model->addPointsForUser($user,$pointsForHighScore[0]->value);
					$this->api_model->addHighScoreActivity($user,$get_game[0]->id, $pointsForHighScore[0]->value);
					//mysql_query("UPDATE ava_highscores  SET score = $score WHERE user = '$user' AND leaderboard = '$leaderboard'") or die (mysql_error);
				}
			}
			else{
					
				$this->api_model->addHighScore($get_game[0]->id , $score, $user, $date);
				$pointsForHighScore = $this->api_model->getPointsFor('highscore');
				$this->api_model->addHighScoreActivity($user,$get_game[0]->id, $pointsForHighScore[0]->value);
				$this->api_model->addPointsForUser($user,$pointsForHighScore[0]->value);
				//	mysql_query("INSERT INTO ava_highscores (game, score, user, date, leaderboard) VALUES ($get_game[0]->id, $score, $user, '$date', '$leaderboard')") or die (mysql_error);

			}
			
			$this->api_model->addRecordToPlay($user, $get_game[0]->id, $this->getIP());
			$pointsForPlay = $this->api_model->getPointsFor('play');
			$this->api_model->addPointsForUser($user,$pointsForPlay[0]->value);
			$this->api_model->plusOneHitsForGame($get_game[0]->id);
			$this->api_model->addPlayActivity($user, $get_game[0]->id, $pointsForPlay[0]->value);
		}
		else
		{
			echo $hashed." not equile to ".$myhash;
		}
	}

	public function addTakoSave()
	{
		if(!isset($this->user_model)) $this->load->model('user_model', "", true);
			
		$salt = "3ammom7md"; //our test salt
			
		//standard bridge parameters
		$user = $_POST['userID'];
		$username = $_POST['username'];
		$leaderboard = $_POST['boardID'];
		$save = $_POST['data'];
		$get_game = $this->user_model->get_game_by_tako($leaderboard); // mysql_fetch_array(mysql_query("SELECT * FROM ava_games WHERE tako_id = '$gametag'"));
		$date = date("Y-m-d H:i:s");
		$hashed = $_POST['hashed'];
			
		$hashphrase = $salt.$save.$user;
			
		//echo $hashed." not equile to ".$hashphrase;
			
		$myhash = sha1($hashphrase);
			
			
		if($hashed == $myhash)
		{

			$dbdata = $this->user_model->getScore($user , $leaderboard);

			$dbdata = ($dbdata && $dbdata[0]->save)? $dbscore[0]->save : 0;

			if($dbscore){

				$this->user_model->updateSave($save , $user , $leaderboard);
				//mysql_query("UPDATE ava_highscores  SET score = $score WHERE user = '$user' AND leaderboard = '$leaderboard'") or die (mysql_error);
			}
			else{
				$this->user_model->addSave($get_game[0]->id , $save, $user, $date,  $leaderboard);
				//	mysql_query("INSERT INTO ava_highscores (game, score, user, date, leaderboard) VALUES ($get_game[0]->id, $score, $user, '$date', '$leaderboard')") or die (mysql_error);
			}
			
			
		}
		else
		{
			echo $hashed." not equile to ".$myhash;
		}
	}

	public function getData()
	{
		if(!isset($this->user_model)) $this->load->model('user_model', "", true);
		if(!isset($_POST['user_id']) || !isset($_POST['game_id']))
		echo json_encode(array('result' => 'error' , 'msg' => 'Either user or game was not passed correctly!'));
		else
		{
			$score = $this->user_model->getMaxScoreByUserIdAndGameId($_POST['user_id'] , $_POST['game_id']);
			echo json_encode(array('result' => 'success' , 'score' => $score));
		}
	}


function sharegameonfb() {
		if(!isset($this->api_model)) $this->load->model('api_model', "", true);
		
		$user_id = $this->session->userdata('user_id');
		$game_id = $_POST['game_id'];
		if(!empty($user_id) && !empty($game_id))
		{
			$this->api_model->addFacebookShare($user_id, $game_id, $this->getIP());
			
			$pointsForShare = $this->api_model->getPointsFor('share');
			$this->api_model->addPointsForUser($user_id, $pointsForShare[0]->value);
			$this->api_model->addShareActivity($user_id, $game_id, $pointsForShare[0]->value);
		}
		
	}

      
}

