
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//game controller  

class Game_func extends CI_Controller {

	 
	                
	function __construct()
	{
 		parent::__construct();
		// loading the helpers 
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->helper('user');
		$this->load->helper('url');
		
		//load models 
		$this->load->model('game_model', "", true);
		$this->load->model('site_model', "", true);
		$this->load->model('user_model', "", true);
		$this->load->model('adv', "", true);
		
	
	}
	
	public function index($seoname=TRUE)
	{
     echo $seoname;
	 
		
	}

		

 
 
 /// view the game function (the main page of the game)
 /// URL / Game / Play / Game Name (Added Play to be better for SEO )

/////////////////////////Arabic URL 
		
	public function العب($name)
	{
		 $r= 'game/';
		 $r.=$name;
		 
		 redirect($r);
	}
	
///////////////////////// English URL 

	public function play($name)
	{

		// check if the user logged in 
		$session_id = $this->session->userdata('username');
		
		// Get all sessions 
		$data["user_session"]=$this->session->all_userdata();
		
		//if the user not logged in 
		if (!$session_id)
		{
			$data["user_session"]['logged_in']="FALSE";
			$data["user_session"]['name']=0;
			$data["user_session"]['facebookid']=0;
			$data["user_session"]['login_type']="Not Logged";
			$data["user_session"]['username']="زائر";
		}
		
	    ///
		$facebookid=$data["user_session"]['facebookid'];
		
       /// find the IP address 
	   $ipa=$_SERVER['REMOTE_ADDR'];
	   $data["user_session"]['ipa']=$ipa;
		
      //get the current user ID
	  $username=$this->session->userdata('username');
	  $login_type=$this->session->userdata('login_type');
	  $user_id=0;
	  $user_image="none";
	  
	  if ($login_type=="Normal")
	  {
	   
	   $user_data=$this->user_model->get_user_id($username);
	   
	   foreach ($user_data as $userdata )
	   {
	   $user_id=$userdata->id;
	   $user_image=$userdata->profile_image_url;
	   }
	 }

     //////if login through facebook 
	 
      if ($login_type=="FACEBOOK")
	  {
	   
	   $user_data_facebook=$this->user_model->get_user_id_facebook($facebookid);
	   
	   foreach ($user_data_facebook as $userdata )
	   {
	   $user_id=$userdata->user_id;
	   $user_image=$userdata->profile_image_url;
	   	   
	   }
	
	
	 }
	 
	 ///Set User ID and image 
	 $data["user_id"]=$user_id;
	 $data["user_image"]=$user_image;
	 
	 
	 
	/// get this game data 
		
	$data['game']= $this->game_model->get_this_game($name);
	$game_id=0;
	//get game id 
      foreach ($data['game'] as $gameid)
		{
			$game_id=$gameid->id;
			// finding the meta descr
		$metades=$name." لعبة جميلة العبها" .$gameid->tags; 
		   $data['metades']=$metades;
		   
		   $data['metatitle']=$gameid->name;


		}
	
	// if no game selected 
	if ($game_id==0)
	{
		redirect("/game/wrong_name");
	}	
	
	/// get the comments 
	$data["comments_data"]=$this->game_model->get_comments_games($game_id);
	
	
	/// get random games 
	    
	 $data["randgames"]=$this->game_model->get_random_games(7);	
	 $data["randgames2"]=$this->game_model->get_random_games(7);
		
	//check if the user can rate this game 
	$can_rate=1;
	
	$check_rate=$this->game_model->check_rate($user_id , $game_id);
	
	foreach ($check_rate as $yescan)
	{
		$can_rate=0;
	}
	
	$data["can_rate"]=$can_rate;
	
	
	
	///check if the user can add this game to his list 
	
	$can_add=1;
	
	$check_list=$this->game_model->check_list($user_id , $game_id);
	
	foreach ($check_list as $yescanadd)
	{
		$can_add=0;
	}
	
	$data["can_add"]=$can_add;
		
	//get the leader adv	
	$data['adv']= $this->adv->get_this_game_adv($game_id);
	
	//Loding the view 
		
	  $this->load->view('layouts/homepage', $data);
	  $data['main_cat'] = $this->site_model->get_main_cats();
	  $this->load->view('activity/activity', $data);
	  $this->load->view('layouts/header', $data);
      $this->load->view('game/view_game', $data);
	  $this->load->view('layouts/footer');
		
		
		
} // end of play game 


//////////////////////////////////////////////////////
//if some wrote the game name wrong 
/////////////////////////////////////////////////////

public function wrong_name()
{
	
	
	// check if the user logged in 
		$session_id = $this->session->userdata('username');
		
		// Get all sessions 
		$data["user_session"]=$this->session->all_userdata();
		
		//if the user not logged in 
		if (!$session_id)
		{
			$data["user_session"]['logged_in']="FALSE";
			$data["user_session"]['name']=0;
			$data["user_session"]['facebookid']=0;
			$data["user_session"]['login_type']="Not Logged";
			$data["user_session"]['username']="زائر";
		}
		
	    ///
		$facebookid=$data["user_session"]['facebookid'];
		
       /// find the IP address 
	   $ipa=$_SERVER['REMOTE_ADDR'];
	   $data["user_session"]['ipa']=$ipa;
		
      //get the current user ID
	  $username=$this->session->userdata('username');
	  $login_type=$this->session->userdata('login_type');
	  $user_id=0;
	  $user_image="none";
	  
	  if ($login_type=="Normal")
	  {
	   
	   $user_data=$this->user_model->get_user_id($username);
	   
	   foreach ($user_data as $userdata )
	   {
	   $user_id=$userdata->id;
	   $user_image=$userdata->profile_image_url;
	   }
	 }

     //////if login through facebook 
	 
      if ($login_type=="FACEBOOK")
	  {
	   
	   $user_data_facebook=$this->user_model->get_user_id_facebook($facebookid);
	   
	   foreach ($user_data_facebook as $userdata )
	   {
	   $user_id=$userdata->user_id;
	   $user_image=$userdata->profile_image_url;
	   	   
	   }
	
	
	 }
	 
	 ///Set User ID and image 
	 $data["user_id"]=$user_id;
	 $data["user_image"]=$user_image;
	 
	 //Loding the view 
	  $data["wrong"]="هذه العبة غير موجدة";
	  $this->load->view('layouts/homepage');
	  $this->load->view('layouts/header', $data);
      $this->load->view('wrong', $data);
	  $this->load->view('layouts/footer');
	 
}
/////////////////////////////////////////////////////

public function addcomments ($id, $user_id, $ipa, $name )
{

    $username=$this->session->userdata('username');
	if ($user_id==0){$login_type="Not Logged";$username="زائر";	$user_image="http://www.gametako.com/css/img/gametako_logo.png";}
	
	
	$comment= $this->input->post('commentarea');	
	
	$login_type=$this->session->userdata('login_type');
	
	  
	  if ($login_type=="Normal")
	  {
	   
	   $user_data=$this->user_model->get_user_id($username);
	   
	   foreach ($user_data as $userdata )
	   {
	   $username=$userdata->username;
	   //$user_image=$userdata->profile_image_url;
	   }
	 }

     //////if login through facebook 
	 
      if ($login_type=="FACEBOOK")
	  {
	   
	   $user_data_facebook=$this->user_model->get_user_id_facebook($facebookid);
	   
	   foreach ($user_data_facebook as $userdata )
	   {
	   $username=$userdata->email;
	   //$user_image=$userdata->profile_image_url;

	   }
	 }
	 

	 
	 
	$comment_id=$this->game_model->add_comment($id, $user_id, $ipa, $comment , $name, $username,  "none" );
    /// we want to add to the user table that he added comments 
	$comments_points=$this->game_model->add_comments_points($user_id);
	
	///////////////////////////////////////////////////////////////////////////////
	$r="/game/";
	$r.=$name;
	
	redirect($r);
} 
 // like function 
 public function like ($id, $user_id, $ipa, $name )
 
{
	
	// insert into rating table 
	$rate_id=$this->game_model->add_rate($id, $user_id, $ipa, 5 , $name );
	// find the points to be added to the user for like 
	
	$pointsdata=$this->game_model->find_points("rate");
	
	$points=0;
	 foreach ($pointsdata as $info )
	   {
	   $points=$info->value;
	   }
     
	 
	///add points 
	
	$addpoints=$this->game_model->add_points( $points, $user_id);

	
	$newnlike=$this->game_model->find_current_like($id);
	
	
	//Changing the content of the data 
	
	$cont= $newnlike;
	$cont.="&nbsp;<input  type=image src='";
	$cont.=base_url();
	$cont.="/css/img/icons/like.png' >";
	
	echo $cont;
	 
}

//show like 

 public function showlike ($id )
 
{
	
	
	$newnlike=$this->game_model->find_like($id);
	
	
	//Changing the content of the data 
	
	$cont= $newnlike;
	$cont.="&nbsp;<input  type=image src='";
	$cont.=base_url();
	$cont.="/css/img/icons/like.png' >";
	
	echo $cont;
	 
}

 // show dislike

function showdislike($id)
{
	$new_ndislik=$this->game_model->find_dislike($id);
	
	
	//Changing the content of the data 
	

	$cont="<input  type=image src='";
	$cont.=base_url();
	$cont.="/css/img/icons/dislike.png'>&nbsp;";
	$cont.= $new_ndislik;
	
	echo $cont;
}
 // dislike function 
 public function dislike ($id, $user_id, $ipa, $name )
 
{
	
	// insert into rating table 
	$rate_id=$this->game_model->add_rate($id, $user_id, $ipa, 0 , $name );
	
		// find the points to be added to the user for rate 
	
	$pointsdata=$this->game_model->find_points("rate");
	
	$points=0;
	 foreach ($pointsdata as $info )
	   {
	   $points=$info->value;
	   }

	///add points 
	
	$addpoints=$this->game_model->add_points( $points, $user_id);
	
	
	$new_ndislik=$this->game_model->find_current_dislike($id);
	
	
	//Changing the content of the data 
	

	$cont="<input  type=image src='";
	$cont.=base_url();
	$cont.="/css/img/icons/dislike.png'>&nbsp;";
	$cont.= $new_ndislik;
	
	echo $cont;
	 
}

// Add to my list 

public function addtomyfav($id, $user_id,  $name )

{
// insert into fav table 
	$list_id=$this->game_model->add_fave($id, $user_id, $name );
	
	
	//Changing the content of the data 
	

	$cont="<input  type=image src='";
	$cont.=base_url();
	$cont.="/css/img/icons/fav_not.png' >";

	
	echo $cont;
}


/// show fav 

public function showfav($id, $user_id,  $name )

{

	
	
	//Changing the content of the data 
	

	$cont="<input  type=image src='";
	$cont.=base_url();
	$cont.="/css/img/icons/fav_not.png' >";

	
	echo $cont;
}

/////////////////////////////////////////////////////////////////
/////
// Add to Friends

public function addtomyfriends($profile_user_id, $my_id, $name)

{
// insert into fav table 
	$list_id=$this->game_model->add_fr($profile_user_id, $my_id, $name );
	
	
	//Changing the content of the data 
	

	$cont="تم الاضافة";

	
	echo $cont;
}



//////////////////////////////////////////////////////////////////	
    // admin or developer addgame 
	public function addgame()
	{
		
		    $this->load->helper('my_form_helper');
		    $data['dropdown'] = form_dropdown_from_db('category_id', "SELECT id,name FROM cats");   
			$data['types'] = form_dropdown_from_db('type', "SELECT extn,type FROM types"); 
	 		$this->load->view('addgame', $data );

	}
	
	/// home page slider
	public function slider()
	{
		
		   if(!isset($this->game_model)) $this->load->model('game_model', "", true);

	     	$data['query'] = $this->game_model->get_slider_games();
         
	 		$this->load->view('homepage/slider',$data);

	}
	
	// get the new games 
	public function newgames()
	{
		if(!isset($this->game_model)) $this->load->model('game_model', "", true);

		$data['query'] = $this->game_model->get_new_games();
         
		$this->load->view('games/newgames',$data );
		
		
	}
	

	public function get_all_scores()
	{
		$query = $this->game_model->get_all_scores($_GET['tako_id']);
		echo $query;
	}

	public function logout()
	{
		unset($_SESSION);
		session_destroy();
		$this->load->view('game_view');
		header("Location: " . base_url());

	}
	


	public function mygamesFeed()
	{
		if(!isset($this->user_model)) $this->load->model('user_model', "", true);

		$myGames = $this->user_model->get_my_games($_SESSION['user'][0]->id);

		echo json_encode(array('games' => $myGames));
	}

	public function getMaxscore()
	{
		if(!isset($this->user_model)) $this->load->model('user_model', "", true);
		if(!isset($_POST['user_id']) || !isset($_POST['game_id']))
		echo json_encode(array('result' => 'error' , 'msg' => 'Either user or game was not passed correctly!'));

		$score = $this->user_model->getMaxScoreByUserIdAndGameId($_POST['user_id'] , $_POST['game_id']);
		echo json_encode(array('result' => 'success' , 'score' => $score));
	}

	public function getMaxScoreInsideGame()
	{
		if(!isset($this->user_model)) $this->load->model('user_model', "", true);
		if(!isset($_POST['userID']) || !isset($_POST['boardID']))
		echo json_encode(array('result' => 'error' , 'msg' => 'Either user or tako id was not passed correctly!'));
		$score = $this->user_model->getMaxScore($_POST['userID'] , $_POST['boardID']);
		$score = ($score && $score[0]->score)? $score[0]->score : -1;
		echo json_encode(intval($score));
	}
	
	public function getSaveInsideGame()
	{
		if(!isset($this->user_model)) $this->load->model('user_model', "", true);
		if(!isset($_POST['userID']) || !isset($_POST['boardID']))
		echo json_encode(array('result' => 'error' , 'msg' => 'Either user or tako id was not passed correctly!'));
		$score = $this->user_model->getMaxSave($_POST['userID'] , $_POST['boardID']);
		$score = ($score && $score[0]->data)? $score[0]->data : -1;
		echo $score;
	}

	public function addTakoHighscore()
		{
			if(!isset($this->user_model)) $this->load->model('user_model', "", true);
	
			$salt = "3ammom7md"; //our test salt
	
			//standard bridge parameters
			$user = $_POST['userID'];
			$username = $_POST['username'];
			$leaderboard = $_POST['boardID'];
			$score = $_POST['score'];
			$get_game = $this->user_model->get_game_by_tako($leaderboard); // mysql_fetch_array(mysql_query("SELECT * FROM ava_games WHERE tako_id = '$gametag'"));
			$date = date("Y-m-d H:i:s");
			$hashed = $_POST['hashed'];
	
			$hashphrase = $salt.$score.$user;
	
			//echo $hashed." not equile to ".$hashphrase;
	
			$myhash = sha1($hashphrase);
	
	
			if($hashed == $myhash)
			{
	
				$dbscore = $this->user_model->getMaxScore($user , $leaderboard);
				
				$dbscore = ($dbscore && $dbscore[0]->score)? $dbscore[0]->score : 0;
				
				if($dbscore){
					if ( $dbscore < $score){
						$this->user_model->updateHighScore($score , $user , $leaderboard);
						//mysql_query("UPDATE ava_highscores  SET score = $score WHERE user = '$user' AND leaderboard = '$leaderboard'") or die (mysql_error);
					}
				}
				else{
					$this->user_model->addHighScore($get_game[0]->id , $score, $user, $date,  $leaderboard);
					//	mysql_query("INSERT INTO ava_highscores (game, score, user, date, leaderboard) VALUES ($get_game[0]->id, $score, $user, '$date', '$leaderboard')") or die (mysql_error);
	
				}
			}
			else
			{
				echo $hashed." not equile to ".$myhash;
			}
		}
	
	public function addTakoSave()
		{
				if(!isset($this->user_model)) $this->load->model('user_model', "", true);
			
					$salt = "3ammom7md"; //our test salt
			
					//standard bridge parameters
					$user = $_POST['userID'];
					$username = $_POST['username'];
					$leaderboard = $_POST['boardID'];
					$save = $_POST['data'];
					$get_game = $this->user_model->get_game_by_tako($leaderboard); // mysql_fetch_array(mysql_query("SELECT * FROM ava_games WHERE tako_id = '$gametag'"));
					$date = date("Y-m-d H:i:s");
					$hashed = $_POST['hashed'];
			
					$hashphrase = $salt.$save.$user;
			
					//echo $hashed." not equile to ".$hashphrase;
			
					$myhash = sha1($hashphrase);
			
			
					if($hashed == $myhash)
					{
			
						$dbdata = $this->user_model->getScore($user , $leaderboard);
						
						$dbdata = ($dbdata && $dbdata[0]->save)? $dbscore[0]->save : 0;
						
						if($dbscore){
						
								$this->user_model->updateSave($save , $user , $leaderboard);
								//mysql_query("UPDATE ava_highscores  SET score = $score WHERE user = '$user' AND leaderboard = '$leaderboard'") or die (mysql_error);
						}
						else{
							$this->user_model->addSave($get_game[0]->id , $save, $user, $date,  $leaderboard);
							//	mysql_query("INSERT INTO ava_highscores (game, score, user, date, leaderboard) VALUES ($get_game[0]->id, $score, $user, '$date', '$leaderboard')") or die (mysql_error);
			
						}
			}
					else
					{
						echo $hashed." not equile to ".$myhash;
					}
	}

	public function getData()
	{
		if(!isset($this->user_model)) $this->load->model('user_model', "", true);
		if(!isset($_POST['user_id']) || !isset($_POST['game_id']))
		echo json_encode(array('result' => 'error' , 'msg' => 'Either user or game was not passed correctly!'));
		else
		{
			$score = $this->user_model->getMaxScoreByUserIdAndGameId($_POST['user_id'] , $_POST['game_id']);
			echo json_encode(array('result' => 'success' , 'score' => $score));
		}
	}

	public function deleteGame()
	{
		if(!isset($this->user_model)) $this->load->model('user_model', "", true);
		if(!isset($_POST['game_id']))
		echo json_encode(array('result' => 'error' , 'error' => 'Game was not passed correctly!'));
		else
		{
			$delete = $this->user_model->deleteGame($_POST['game_id']);
			if($delete)
			echo json_encode(array('result' => 'success'));
			else
			echo json_encode(array('result' => 'error' , 'error' => 'Unable To Delete Game!'));
		}




	}

	public function updatePassword()
	{
		if(!isset($this->user_model)) $this->load->model('user_model', "", true);
		if(!isset($_POST['oldPassword']))
		{
			echo json_encode(array('result' => 'error', 'error' => 'Old Password was not sent correctly!'));
			return;
		}

		if(!isset($_POST['newPassword']))
		{
			echo json_encode(array('result' => 'error' , 'error' => 'New Password was not sent correctly!'));
			return;
		}

		if(!isset($_POST['confirmPassword']))
		{
			echo json_encode(array('result' => 'error' , 'error' => 'Confirm Password was not sent correctly!'));
			return;
		}

		if($_POST['newPassword'] !== $_POST['confirmPassword'])
		{
			echo json_encode(array('result' => 'error', 'error' => 'New Password does not match with Confirm Password'));
			return;
		}

		if(strlen($_POST['newPassword']) < 4)
		{
			echo json_encode(array('result' => 'error' , 'error' => 'New Password length must be at least 4 characters'));
			return;
		}

		$newPassword = $this->user_model->updatePassword($_POST['oldPassword'], $_POST['newPassword'], $_SESSION['user'][0]->id);

		if(!$newPassword)
		{
			echo json_encode(array('result' => 'error' , 'error' => 'Un-Expected Error! Please Try again later'));
			return;
		}
		else if(!isset($newPassword['error']))
		echo json_encode(array('result' => 'success'));
		else
		 echo json_encode(array('result' => 'error' , 'error' => $newPassword['error']));
	}

        function mygamestakoid() {
        $result = array('success' => false, 'data' => null, 'error' => array());
        if(!isset($this->user_model)) $this->load->model('user_model', "", true);
        if(!isset($_SESSION['user'])) {$result['error'][] = 'user is not logged in'; echo json_encode($result); return;}

        $takos = $this->user_model->getTakosByDevId($_SESSION['user'][0]->id);
        
        if($takos)
        {
          $data = array();
          foreach($takos as $tako) 
          { $data[]['value']  = $tako->tako_id; }
          $result['success'] = true;
          $result['data'] = $data;
        }
        echo json_encode($result);



        }
		
		
		
		////////////////////////////////get the random game 
		
		function random_one()
		{
		
		 $rs=$this->game_model->get_random_games_1();
		 
		 foreach ($rs as $games)
		 {
			 $name=$games->name;
		 }	
          
		  $r="/game/";
		  $r.=$name;
		  		
		  redirect($r);

			
			
		}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
