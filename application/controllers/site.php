<?
/// controlling site home page 
///NSH 2012
?>

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site extends CI_Controller {

	 
	                
	function __construct()
	{
 		parent::__construct();
		
		$this->load->model('site_model');
		
		$this->load->model('game_model');
		$this->load->model('user_model');
		
		$this->load->library('form_validation');
		
		$this->load->helper('form');
		
		$this->load->helper('url');
		
		$this->load->library('MY_Layout');
	
	}
	
	//public function index()
	///{
	//	$this->load->model('game_model', "", true);
    //    redirect('site/show_all_home');
		
	//}

	
	public function homepage()
	{
			if(!isset($this->game_model)) $this->load->model('game_model', "", true);

	     	$data['slider'] = $this->game_model->get_slider_games();
         
	 		$this->load->view('homepage/slider',$data);
			
			
			$data['new'] = $this->game_model->get_new_games();
         
		 
		    $this->load->view('games/newgames',$data );
			
	}
	
	public function showheader()
	{
			   
	 	  
		 $this->load->view('layouts/homepage');
		 $data['main_cat'] = $this->site_model->get_main_cats();
		 $this->load->view('layouts/header', $data);
			
			
			
			
	}
	
	///
	public function invited($username)
	{
		// insert the data into activities 
		///
		redirect("site/show_all_home");
	}
  
  // show all home 
  
  
  // show all home 
public function index()  {
    
	$thishome=1;
	$data['thishome']=$thishome;

	if(!isset($this->site_model)) $this->load->model('site_model', "", true);
	  
	$this->my_layout->header = 'layouts/homepage';
	
     // or turn on footer
    $this->my_layout->footer = 'layouts/footer';

	// loading the data 
	///site main vategories 
	$data['main_cat'] = $this->site_model->get_main_cats();
    // slider data 
	$data['slider'] = $this->game_model->get_slider_games();
	/// new games
	$data['new'] = $this->game_model->get_new_games(7);
	// top rated games 
	$data['top'] = $this->game_model->get_top_games(7);
	//most viewed games 
	$data['mostviews'] = $this->game_model->get_mostviews_games(7);
	// featured games 
	$data['rec'] = $this->game_model->get_rec_games(7);
	//other games 
	$data['others'] = $this->game_model->get_other_games();
	// site title 
	$data['title'] = 'موقع تاكو للالعاب';
	
	
	///////////////////////////GET current user data //////////////////////////////////////////////
	// check if the user logged in 
		$session_id = $this->session->userdata('username');
		
		// Get all sessions 
		$data["user_session"]=$this->session->all_userdata();
		
		//if the user not logged in 
		if (!$session_id)
		{
			$data["user_session"]['logged_in']="FALSE";
			$data["user_session"]['name']=0;
			$data["user_session"]['facebookid']=0;
			$data["user_session"]['login_type']="Not Logged";
			$data["user_session"]['username']="زائر";
		}
		
	    ///
		$facebookid=$data["user_session"]['facebookid'];
		
       /// find the IP address 
	   $ipa=$_SERVER['REMOTE_ADDR'];
	   $data["user_session"]['ipa']=$ipa;
		
      //get the current user ID
	  $username=$this->session->userdata('username');
	  $login_type=$this->session->userdata('login_type');
	  $user_id=0;
	  $user_image="none";
	  
	  if ($login_type=="Normal")
	  {
	   
	   $user_data=$this->user_model->get_user_id($username);
	   
	   foreach ($user_data as $userdata )
	   {
	   $user_id=$userdata->id;
	   $user_image=$userdata->profile_image_url;
	   }
	 }

     //////if login through facebook 
	 
      if ($login_type=="FACEBOOK")
	  {
	   
	   $user_data_facebook=$this->user_model->get_user_id_facebook($facebookid);
	   
	   foreach ($user_data_facebook as $userdata )
	   {
	   $user_id=$userdata->user_id;
	   $user_image=$userdata->profile_image_url;
	   	   
	   }
	
	
	 }
	 
	 ///Set User ID and image 
	 $data["user_id"]=$user_id;
	 $data["user_image"]=$user_image;
	 
	 
	
	
	//////////////////////////////End get current user data ///////////////////////////////////////
	
	// loading the layout 
	
	 $this->load->view("layouts/homepage", $data); 
	 $this->load->view("layouts/header", $data );
	 $this->load->view('activity/activity', $data);
	 $this->load->view("homepage/slider", $data );
	 $this->load->view("games/newgames", $data );
	 $this->load->view("games/toprated", $data );
	 $this->load->view("games/mostviews", $data );
	 $this->load->view("games/recgames", $data );
	 $this->load->view("layouts/footer", $data );



  }
	/// home page slider
	public function slider()
	{
		
		   if(!isset($this->game_model)) $this->load->model('game_model', "", true);

	     	$data['slider'] = $this->game_model->get_slider_games();
         
	 		$this->load->view('homepage/slider',$data);
			

	}
	
	// get the new games 
	public function newgames()
	{
		if(!isset($this->game_model)) $this->load->model('game_model', "", true);
          
		
		    
		$data['new'] = $this->game_model->get_new_games();
		 
	    $this->load->view('games/newgames',$data );
		
		
	}
	

	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
