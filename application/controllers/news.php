<?
class News extends CI_Controller {

	 
	                
	function __construct()
	{
 		parent::__construct();
		
		$this->load->database();
		$this->load->helper('form');
		$this->load->helper('user');
		$this->load->helper('url');
		
		//load models 
		$this->load->model('game_model', "", true);
		$this->load->model('site_model', "", true);
	    $this->load->model('static_pages', "", true);
		$this->load->model('user_model', "", true);
		$this->load->model('adv', "", true);
	
	}
	
	public function index()
	{
	//
	
	// 
	}

		
    public function browse($name=TRUE)
    {
		
		/// get  details ///////////////////////////////////////
		///////////////////////////////////////////////////////////////
		$page_id=0;
		$det=$this->static_pages->get_news_seo($name);
		
		foreach ($det as $pages)
		{
		//id 
        $page_id=$pages->id;
		$data['page_id']=$pages->id;
		
		/// find news type 
		$data['type']=$pages->type;
		$type=$pages->type;
		
		/// find the writer
		$data['writer']=$pages->writer;
		$writer=$pages->writer;

		//  name 
		$pages_name=$pages->title;
		$data['pages_name']=$pages->title;
		//description				
		$description=$pages->content;
		$data['description']=$pages->content;
		
			//date				
		$newsdate=$pages->date;
		$data['newsdate']=$pages->date;
		
		//metatags				
		$metatags=$pages->title;
		
		$data['metades']=$pages->title;
		}
		
		////if wrong cat
		if ($page_id==0)
		{
		redirect("/gametako/wrong_name");
		}
		
		
		/////////////////////////////////////////////////////////////////////
		//Find the users data 
		/////////////////////////////////////////////////////////////////////

		 $username= $this->session->userdata('username');				////
   		 $login_type= $this->session->userdata('login_type');   		////
		 $facebookid=$this->session->userdata('facebookid');   	    	////
	   
	    // Get all sessions 
	    $data["user_session"]=$this->session->all_userdata();		   ////
		
    	//if the user not logged in 
		if (!$username)
		{
			$data["user_session"]['logged_in']="FALSE";
			$data["user_session"]['name']=0;
			$data["user_session"]['facebookid']=0;
			$data["user_session"]['login_type']="Not Logged";
			$data["user_session"]['username']="زائر";
			$data["user_id"]=0;
			$facebookid=0;
		}
		
	     $user_image="none";
		 $user_id=0;
	
	     if ($login_type=="Normal")
	     {
	   
	     $user_data=$this->user_model->get_user_id($username);
	      foreach ($user_data as $userdata )
	      {
	       $user_id=$userdata->id;
	       $user_image=$userdata->profile_image_url;
	       }
		   
	     }

     //////if login through facebook 
	 
      if ($login_type=="FACEBOOK")
	  {
	   $user_data_facebook=$this->user_model->get_user_id_facebook($facebookid);
	   foreach ($user_data_facebook as $userdata )
	   {
	   $user_id=$userdata->user_id;
	   $user_image=$userdata->profile_image_url;

	   }
	 }

   		  ///Set User ID and image 
	 	$data["user_id"]=$user_id;
		$data["user_image"]=$user_image;
		$data["logged_in"]=$data["user_session"]['logged_in'];
	
		/////////////////////////////////////////////////////////////////////
		/////End of user data ///////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////
	 	
		/////get the main cats for the header 
	    $data['main_cat'] = $this->site_model->get_main_cats();
	    
		
		
		///////////////////Load Views 
	    if ($type=="GameTako")
		{
		$this->load->view("layouts/homepage", $data );
		 $this->load->view("layouts/header", $data );

	    ///Load cat view 
		$this->load->view("pages/news_det", $data);
	    $this->load->view("layouts/footer");
		}else
		{
		$this->load->view("layouts/homepage_ind", $data );
		 $this->load->view("layouts/header_ind", $data );

	    ///Load cat view 
		$this->load->view("pages/news_det_ind", $data);
	    $this->load->view("layouts/footer");
		}
		
	} // end of browse function 
 
 
 
 
 
 
 
 
 public function wrong_name()
 { 
 
 echo "لقد قمت بدخول تصنيف خاطئ";
 }
//////////////////////


		
 public function browse_all()
    {
		
		/// get  details ///////////////////////////////////////
		///////////////////////////////////////////////////////////////
		$page_id=0;
		
		$news=$this->static_pages->get_news_all(10,"GameTako");
		
		$data["news"]=$news;
		
		
		
		
		/////////////////////////////////////////////////////////////////////
		//Find the users data 
		/////////////////////////////////////////////////////////////////////

		 $username= $this->session->userdata('username');				////
   		 $login_type= $this->session->userdata('login_type');   		////
		 $facebookid=$this->session->userdata('facebookid');   	    	////
	   
	    // Get all sessions 
	    $data["user_session"]=$this->session->all_userdata();		   ////
		
    	//if the user not logged in 
		if (!$username)
		{
			$data["user_session"]['logged_in']="FALSE";
			$data["user_session"]['name']=0;
			$data["user_session"]['facebookid']=0;
			$data["user_session"]['login_type']="Not Logged";
			$data["user_session"]['username']="زائر";
			$data["user_id"]=0;
			$facebookid=0;
		}
		
	     $user_image="none";
		 $user_id=0;
	
	     if ($login_type=="Normal")
	     {
	   
	     $user_data=$this->user_model->get_user_id($username);
	      foreach ($user_data as $userdata )
	      {
	       $user_id=$userdata->id;
	       $user_image=$userdata->profile_image_url;
	       }
		   
	     }

     //////if login through facebook 
	 
      if ($login_type=="FACEBOOK")
	  {
	   $user_data_facebook=$this->user_model->get_user_id_facebook($facebookid);
	   foreach ($user_data_facebook as $userdata )
	   {
	   $user_id=$userdata->user_id;
	   $user_image=$userdata->profile_image_url;

	   }
	 }

   		  ///Set User ID and image 
	 	$data["user_id"]=$user_id;
		$data["user_image"]=$user_image;
		$data["logged_in"]=$data["user_session"]['logged_in'];
	
		/////////////////////////////////////////////////////////////////////
		/////End of user data ///////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////
	 	
		/////get the main cats for the header 
	    $data['main_cat'] = $this->site_model->get_main_cats();
	    
		
		
		///////////////////Load Views 
	    $this->load->view("layouts/homepage", $data );
		 $this->load->view("layouts/header", $data );

	    ///Load cat view 
		$this->load->view("pages/news", $data);
	    $this->load->view("layouts/footer");
		
		
	} // end of browse all function 
 




////////////////
public function browse_all_i()
    {
		
		/// get  details ///////////////////////////////////////
		///////////////////////////////////////////////////////////////
		$page_id=0;
		
		$news=$this->static_pages->get_news_all(10,"Indust");
		
		$data["news"]=$news;
		
		
		
		
		/////////////////////////////////////////////////////////////////////
		//Find the users data 
		/////////////////////////////////////////////////////////////////////

		 $username= $this->session->userdata('username');				////
   		 $login_type= $this->session->userdata('login_type');   		////
		 $facebookid=$this->session->userdata('facebookid');   	    	////
	   
	    // Get all sessions 
	    $data["user_session"]=$this->session->all_userdata();		   ////
		
    	//if the user not logged in 
		if (!$username)
		{
			$data["user_session"]['logged_in']="FALSE";
			$data["user_session"]['name']=0;
			$data["user_session"]['facebookid']=0;
			$data["user_session"]['login_type']="Not Logged";
			$data["user_session"]['username']="زائر";
			$data["user_id"]=0;
			$facebookid=0;
		}
		
	     $user_image="none";
		 $user_id=0;
	
	     if ($login_type=="Normal")
	     {
	   
	     $user_data=$this->user_model->get_user_id($username);
	      foreach ($user_data as $userdata )
	      {
	       $user_id=$userdata->id;
	       $user_image=$userdata->profile_image_url;
	       }
		   
	     }

     //////if login through facebook 
	 
      if ($login_type=="FACEBOOK")
	  {
	   $user_data_facebook=$this->user_model->get_user_id_facebook($facebookid);
	   foreach ($user_data_facebook as $userdata )
	   {
	   $user_id=$userdata->user_id;
	   $user_image=$userdata->profile_image_url;

	   }
	 }

   		  ///Set User ID and image 
	 	$data["user_id"]=$user_id;
		$data["user_image"]=$user_image;
		$data["logged_in"]=$data["user_session"]['logged_in'];
	
		/////////////////////////////////////////////////////////////////////
		/////End of user data ///////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////
	 	
		/////get the main cats for the header 
	    $data['main_cat'] = $this->site_model->get_main_cats();
	    
		
		
		///////////////////Load Views 
	    $this->load->view("layouts/homepage_ind", $data );
		 $this->load->view("layouts/header_ind", $data );

	    ///Load cat view 
		$this->load->view("pages/news_ind", $data);
	    $this->load->view("layouts/footer");
		
		
	} // end of browse industry function 
 





///////////////////

 public function browse_others()
    {
		
		/// get  details ///////////////////////////////////////
		///////////////////////////////////////////////////////////////
		$page_id=0;
		$news=$this->static_pages->get_news_all(50,"GameTako");
		
		$data["news"]=$news;
		
		
		
		
		/////////////////////////////////////////////////////////////////////
		//Find the users data 
		/////////////////////////////////////////////////////////////////////

		 $username= $this->session->userdata('username');				////
   		 $login_type= $this->session->userdata('login_type');   		////
		 $facebookid=$this->session->userdata('facebookid');   	    	////
	   
	    // Get all sessions 
	    $data["user_session"]=$this->session->all_userdata();		   ////
		
    	//if the user not logged in 
		if (!$username)
		{
			$data["user_session"]['logged_in']="FALSE";
			$data["user_session"]['name']=0;
			$data["user_session"]['facebookid']=0;
			$data["user_session"]['login_type']="Not Logged";
			$data["user_session"]['username']="زائر";
			$data["user_id"]=0;
			$facebookid=0;
		}
		
	     $user_image="none";
		 $user_id=0;
	
	     if ($login_type=="Normal")
	     {
	   
	     $user_data=$this->user_model->get_user_id($username);
	      foreach ($user_data as $userdata )
	      {
	       $user_id=$userdata->id;
	       $user_image=$userdata->profile_image_url;
	       }
		   
	     }

     //////if login through facebook 
	 
      if ($login_type=="FACEBOOK")
	  {
	   $user_data_facebook=$this->user_model->get_user_id_facebook($facebookid);
	   foreach ($user_data_facebook as $userdata )
	   {
	   $user_id=$userdata->user_id;
	   $user_image=$userdata->profile_image_url;

	   }
	 }

   		  ///Set User ID and image 
	 	$data["user_id"]=$user_id;
		$data["user_image"]=$user_image;
		$data["logged_in"]=$data["user_session"]['logged_in'];
	
		/////////////////////////////////////////////////////////////////////
		/////End of user data ///////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////
	 	
		/////get the main cats for the header 
	    $data['main_cat'] = $this->site_model->get_main_cats();
	    
		
		
		///////////////////Load Views 
	    $this->load->view("layouts/homepage", $data );
		 $this->load->view("layouts/header", $data );

	    ///Load cat view 
		$this->load->view("pages/news", $data);
	    $this->load->view("layouts/footer");
		
		
	} // end of browse all function 

//////////////////////
}// End of news Controller 
