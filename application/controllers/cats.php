<?
/// This category controller will control all categories 
////////////////////N SH 2012 //////////////////////////

class Cats extends CI_Controller {

	 
	                
	function __construct()
	{
 		parent::__construct();
		///////////////Loading the needed helpers 
		$this->load->helper('form');
		$this->load->helper('user');
		$this->load->helper('url');
		
		///////////////Loading the needed models
		$this->load->model('game_model', "", true);
		$this->load->model('site_model', "", true);
	    $this->load->model('category', "", true);
		$this->load->model('user_model', "", true);
		$this->load->model('adv', "", true);
	
	}
	
	public function index($name = TRUE)
	{
	//
    redirect("/cats/browse/".$name);
	

	// 
	}

	/// browse the main category page 
		
    public function browse($name=TRUE)
    {
		
		/// get category details ///////////////////////////////////////
		///////////////////////////////////////////////////////////////
		$cat_id=0;
		$cat_det=$this->category->get_details_seo($name);
		foreach ($cat_det as $cats)
		{
		//cat id 
        $cat_id=$cats->id;
		$data['cat_id']=$cats->id;
		// cat name 
		$cat_name=$cats->name;
		$data['cat_name']=$cats->name;
		//description				
		$description=$cats->description;
		$data['description']=$cats->description;
		//Meta Descr 
		
		//$metades=$name."العاب شيقة ".$cats->description;
		//$metades.=$cats->keywords;
		//$data['metades']=$metades;
		  $metades = strip_tags($cats-> description);
	
	      $metades = mb_substr($metades, 0, 255,'UTF8');
		  
		   $data['metades']=$metades;

		}
		
		////if wrong cat
		if ($cat_id==0)
		{
		redirect("/cats/wrong_name");
		}
		///////Find the games for this category /////////////////////////////
		/////////////////////////////////////////////////////////////////////
		$cat_games=$this->category->get_games_cat($cat_id);
		$data["cat_games"]=$cat_games;
		///find the number of games 
		$numberofgames = 0; 
		foreach ($cat_games as $number)
		{
			$numberofgames=$numberofgames+1;
	   }
		$data['numberofgames']=$numberofgames;
	
		/////////////////////////////////////////////////////////////////////
		//Find the users data 
		/////////////////////////////////////////////////////////////////////

		 $username= $this->session->userdata('username');				////
   		 $login_type= $this->session->userdata('login_type');   		////
		 $facebookid=$this->session->userdata('facebookid');   	    	////
	   
	    // Get all sessions 
	    $data["user_session"]=$this->session->all_userdata();		   ////
		
    	//if the user not logged in 
		if (!$username)
		{
			$data["user_session"]['logged_in']="FALSE";
			$data["user_session"]['name']=0;
			$data["user_session"]['facebookid']=0;
			$data["user_session"]['login_type']="Not Logged";
			$data["user_session"]['username']="زائر";
			$data["user_id"]=0;
			$facebookid=0;
		}
		
	     $user_image="none";
		 $user_id=0;
	
	     if ($login_type=="Normal")
	     {
	   
	     $user_data=$this->user_model->get_user_id($username);
	      foreach ($user_data as $userdata )
	      {
	       $user_id=$userdata->id;
	       $user_image=$userdata->profile_image_url;
	       }
		   
	     }

     //////if login through facebook 
	 
      if ($login_type=="FACEBOOK")
	  {
	   $user_data_facebook=$this->user_model->get_user_id_facebook($facebookid);
	   foreach ($user_data_facebook as $userdata )
	   {
	   $user_id=$userdata->user_id;
	   $user_image=$userdata->profile_image_url;

	   }
	 }

   		  ///Set User ID and image 
	 	$data["user_id"]=$user_id;
		$data["user_image"]=$user_image;
		$data["logged_in"]=$data["user_session"]['logged_in'];
	
		/////////////////////////////////////////////////////////////////////
		/////End of user data ///////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////
	 	
		/////get the main cats for the header 
	    $data['main_cat'] = $this->site_model->get_main_cats();
	    
		///////////////////Load Views 
		$this->load->view("layouts/homepage", $data );
	    $this->load->view("layouts/header", $data );
	    ///Load cat view 
		$this->load->view("cats/main", $data);
	    $this->load->view("layouts/footer");
		
	} // end of browse function 
 
 
 ///////////////////////////////////////////
 
 	/// browse the main category page / only featured
		
    public function browsef($name=TRUE)
    {
		
		/// get category details ///////////////////////////////////////
		///////////////////////////////////////////////////////////////
		$cat_id=0;
		$cat_det=$this->category->get_details_seo($name);
		foreach ($cat_det as $cats)
		{
		//cat id 
        $cat_id=$cats->id;
		$data['cat_id']=$cats->id;
		// cat name 
		$cat_name=$cats->name;
		$data['cat_name']=$cats->name;
		//description				
		$description=$cats->description;
		$data['description']=$cats->description;
		//Meta Descr 
		//$metades=$name."العاب شيقة ".$cats->description;
	   //	$metades.=$cats->keywords;
		//$data['metades']=$metades;
		
				  $metades = strip_tags($cats-> description);
	
	      $metades = mb_substr($metades, 0, 255,'UTF8');
		  
		   $data['metades']=$metades;

		}
		
		////if wrong cat
		if ($cat_id==0)
		{
		redirect("/cats/wrong_name");
		}
		///////Find the games for this category /////////////////////////////
		/////////////////////////////////////////////////////////////////////
		$cat_games=$this->category->get_games_catf($cat_id);
		$data["cat_games"]=$cat_games;
		///find the number of games 
		$numberofgames = 0; 
		foreach ($cat_games as $number)
		{
			$numberofgames=$numberofgames+1;
	   }
		$data['numberofgames']=$numberofgames;
	
		
		/////////////////////////////////////////////////////////////////////
		//Find the users data 
		/////////////////////////////////////////////////////////////////////

		 $username= $this->session->userdata('username');				////
   		 $login_type= $this->session->userdata('login_type');   		////
		 $facebookid=$this->session->userdata('facebookid');   	    	////
	   
	    // Get all sessions 
	    $data["user_session"]=$this->session->all_userdata();		   ////
		
    	//if the user not logged in 
		if (!$username)
		{
			$data["user_session"]['logged_in']="FALSE";
			$data["user_session"]['name']=0;
			$data["user_session"]['facebookid']=0;
			$data["user_session"]['login_type']="Not Logged";
			$data["user_session"]['username']="زائر";
			$data["user_id"]=0;
			$facebookid=0;
		}
		
	     $user_image="none";
		 $user_id=0;
	
	     if ($login_type=="Normal")
	     {
	   
	     $user_data=$this->user_model->get_user_id($username);
	      foreach ($user_data as $userdata )
	      {
	       $user_id=$userdata->id;
	       $user_image=$userdata->profile_image_url;
	       }
		   
	     }

     //////if login through facebook 
	 
      if ($login_type=="FACEBOOK")
	  {
	   $user_data_facebook=$this->user_model->get_user_id_facebook($facebookid);
	   foreach ($user_data_facebook as $userdata )
	   {
	   $user_id=$userdata->user_id;
	   $user_image=$userdata->profile_image_url;

	   }
	 }

   		  ///Set User ID and image 
	 	$data["user_id"]=$user_id;
		$data["user_image"]=$user_image;
		$data["logged_in"]=$data["user_session"]['logged_in'];
	
		/////////////////////////////////////////////////////////////////////
		/////End of user data ///////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////
	 	
		/////get the main cats for the header 
	    $data['main_cat'] = $this->site_model->get_main_cats();
	    
		///////////////////Load Views 
		$this->load->view("layouts/homepage", $data );
	    $this->load->view("layouts/header", $data );
	    ///Load cat view 
		$this->load->view("cats/main", $data);
	    $this->load->view("layouts/footer");
		
	} // end of browse function 
 
 
 
 
 
 
 ///////////////////////////////////////////
 
 
 ////Wrong name direction 
 
 public function wrong_name()
 { 
 	// check if the user logged in 
		$session_id = $this->session->userdata('username');
		
		// Get all sessions 
		$data["user_session"]=$this->session->all_userdata();
		
		//if the user not logged in 
		if (!$session_id)
		{
			$data["user_session"]['logged_in']="FALSE";
			$data["user_session"]['name']=0;
			$data["user_session"]['facebookid']=0;
			$data["user_session"]['login_type']="Not Logged";
			$data["user_session"]['username']="زائر";
		}
		
	    ///
		$facebookid=$data["user_session"]['facebookid'];
		
       /// find the IP address 
	   $ipa=$_SERVER['REMOTE_ADDR'];
	   $data["user_session"]['ipa']=$ipa;
		
      //get the current user ID
	  $username=$this->session->userdata('username');
	  $login_type=$this->session->userdata('login_type');
	  $user_id=0;
	  $user_image="none";
	  
	  if ($login_type=="Normal")
	  {
	   
	   $user_data=$this->user_model->get_user_id($username);
	   
	   foreach ($user_data as $userdata )
	   {
	   $user_id=$userdata->id;
	   $user_image=$userdata->profile_image_url;
	   }
	 }

     //////if login through facebook 
	 
      if ($login_type=="FACEBOOK")
	  {
	   
	   $user_data_facebook=$this->user_model->get_user_id_facebook($facebookid);
	   
	   foreach ($user_data_facebook as $userdata )
	   {
	   $user_id=$userdata->user_id;
	   $user_image=$userdata->profile_image_url;
	   	   
	   }
	
	
	 }
	 
	 ///Set User ID and image 
	 $data["user_id"]=$user_id;
	 $data["user_image"]=$user_image;
	 
	 //Loding the view 
	  $data["wrong"]="لقد قمت بدخول تصنيف خاطئ";
	  $this->load->view('layouts/homepage');
	  $this->load->view('layouts/header', $data);
      $this->load->view('wrong', $data);
	  $this->load->view('layouts/footer');
 }


 public function top($type=TRUE)
    {
		
		/// get category details ///////////////////////////////////////
		///////////////////////////////////////////////////////////////
		if ($type=='new')
		{
		$data['cat_name']="العاب جـديدة";
		$data['description']="اخر الالعاب الجميلة ";
			//Meta Descr 
		$metades=$data['description'];
		$data['metades']=$metades;
        ///////Find the games for this category /////////////////////////////
		/////////////////////////////////////////////////////////////////////
	    $data["cat_games"]=$this->game_model->get_new_games(50);
		}
		
		
		if ($type=='featured')
		{
		$data['cat_name']="العاب مميزة";
		$data['description']="اخر الالعاب الجميلة ";
			//Meta Descr 
		$metades=$data['description'];
		$data['metades']=$metades;
        ///////Find the games for this category /////////////////////////////
		/////////////////////////////////////////////////////////////////////
	    $data["cat_games"]=$this->game_model->get_rec_games(50);
		}
		
		//top rated 
		if ($type=='rated')
		{
		$data['cat_name']="اعلي الالعاب تقيما ";
		$data['description']="العب الالعاب التي اعجبت الاخرين اكثر  ";
			//Meta Descr 
		$metades=$data['description'];
		$data['metades']=$metades;
        ///////Find the games for this category /////////////////////////////
		/////////////////////////////////////////////////////////////////////
	    $data["cat_games"]=$this->game_model->get_top_games(50);
		}	
		
		
		//top viewed 
		if ($type=='viewed')
		{
		$data['cat_name']="الالعاب الاكثر لعبا ";
		$data['description']="العب اكثر الالعاب لعبا ";
			//Meta Descr 
		$metades=$data['description'];
		$data['metades']=$metades;
        ///////Find the games for this category /////////////////////////////
		/////////////////////////////////////////////////////////////////////
	    $data["cat_games"]=$this->game_model->get_mostviews_games(50);
		}
		/////////////////////////////////////////////////////////////////////
		//Find the users data 
		/////////////////////////////////////////////////////////////////////

		 $username= $this->session->userdata('username');				////
   		 $login_type= $this->session->userdata('login_type');   		////
		 $facebookid=$this->session->userdata('facebookid');   	    	////
	   
	    // Get all sessions 
	    $data["user_session"]=$this->session->all_userdata();		   ////
		
    	//if the user not logged in 
		if (!$username)
		{
			$data["user_session"]['logged_in']="FALSE";
			$data["user_session"]['name']=0;
			$data["user_session"]['facebookid']=0;
			$data["user_session"]['login_type']="Not Logged";
			$data["user_session"]['username']="زائر";
			$data["user_id"]=0;
			$facebookid=0;
		}
		
	     $user_image="none";
		 $user_id=0;
	
	     if ($login_type=="Normal")
	     {
	   
	     $user_data=$this->user_model->get_user_id($username);
	      foreach ($user_data as $userdata )
	      {
	       $user_id=$userdata->id;
	       $user_image=$userdata->profile_image_url;
	       }
		   
	     }

     //////if login through facebook 
	 
      if ($login_type=="FACEBOOK")
	  {
	   $user_data_facebook=$this->user_model->get_user_id_facebook($facebookid);
	   foreach ($user_data_facebook as $userdata )
	   {
	   $user_id=$userdata->user_id;
	   $user_image=$userdata->profile_image_url;

	   }
	 }

   		  ///Set User ID and image 
	 	$data["user_id"]=$user_id;
		$data["user_image"]=$user_image;
		$data["logged_in"]=$data["user_session"]['logged_in'];
	
		/////////////////////////////////////////////////////////////////////
		/////End of user data ///////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////
	 	
		/////get the main cats for the header 
	    $data['main_cat'] = $this->site_model->get_main_cats();
	    
		///////////////////Load Views 
		$this->load->view("layouts/homepage", $data );
	    $this->load->view("layouts/header", $data );
	    ///Load cat view 
	    $this->load->view("cats/main", $data);
	    $this->load->view("layouts/footer");
		
	} // end of browse function 
 
}// End of Cat Controller 
