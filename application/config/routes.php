<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "site";
$route['site'] = "site/index";


$route['404_override'] = '';

//Routing the games 
//$route['game/hiii'] = 'game/كنجز فالي';

$route['game/(:any)'] = 'game/index/$1';



//Routing the profiles 

$route['profile/(:any)'] = 'profile/showprofile/$1';

///static pages 
$route['page/برنامج-تاكو-لسفراء-الكليات-'] = 'gametako/browse/برنامج تاكو لسفراء الكليات';
$route['faq'] = 'gametako/browse/كيف تستخدم مميزات موقع تاكو الألعاب';
$route['task/contact'] = 'gametako/browse/عن الموقع';
$route['page/شروط-و-تعليمات-النشر'] = 'gametako/browse/شروط و-تعليمات النشر';
$route['page/قائمة-المطورين'] = 'gametako/browse/قائمة المطورين';
$route['page/الواجهة-البرمجية-api'] = 'gametako/browse/الواجهة البرمجية API';
$route['news/كيف-تستخدم-مميزات-موقع-تاكو-الألعاب'] = 'gametako/browse/كيف تستخدم مميزات موقع تاكو الألعاب';




///Routing the tags 
$route['tag/(:any)'] = 'tag/browse/$1';

///Routing the news 
$route['news/(:any)'] = 'news/browse/$1';

$route['takonews'] = 'news/browse_all';
$route['industry-news'] = 'news/browse_all_i';


$route['news/others'] = 'news/browse_others';


//// Routing the categories 
$route['admin'] = 'admin/index.php';

$route['ألعاب-صخر'] = 'cats/browse/ألعاب-صخر';

$route['ألعاب-أطفال'] = 'cats/browse/ألعاب-أطفال';

$route['ألعاب-سيارات'] = 'cats/browse/ألعاب-سيارات';

$route['العاب-اكشن'] = 'cats/browse/العاب-اكشن';

$route['العاب-تعليمية'] = 'cats/browse/العاب-تعليمية';

/////////////////////////////////////////////////////
$route['تاكو-ألعاب-صخر'] = 'cats/browsef/ألعاب-صخر';

$route['تاكو-ألعاب-أطفال'] = 'cats/browsef/ألعاب-أطفال';

$route['تاكو-ألعاب-سيارات'] = 'cats/browsef/ألعاب-سيارات';

$route['تاكو-العاب-اكشن'] = 'cats/browsef/العاب-اكشن';

$route['تاكو-العاب-تعليمية'] = 'cats/browsef/العاب-تعليمية';

$route['العاب آخرى'] = 'cats/browse/العاب آخرى';



//////////////////////////////////////////////////////

$route['ألعاب-ألغاز-و-تفكير-و-بازل'] = 'cats/browse/ألعاب-ألغاز-و-تفكير-و-بازل';

$route['العاب-آركيد'] = 'cats/browse/العاب-آركيد';

$route['ألعاب-حيوانات'] = 'cats/browse/ألعاب-حيوانات';

$route['ألعاب-دراجات'] = 'cats/browse/ألعاب-دراجات';

$route['ألعاب-ديكور'] = 'cats/browse/ألعاب-ديكور';

$route['العاب-تلبيس-بنات-فقط'] = 'cats/browse/العاب-تلبيس-بنات-فقط';

$route['العاب-حربية'] = 'cats/browse/العاب-حربية';

$route['العاب-بنات-و-باربي-و-طبخ-و-ترتيب-منزل'] = 'cats/browse/العاب-بنات-و-باربي-و-طبخ-و-ترتيب-منزل';

$route['ألعاب-ورق-و-كتشينة'] = 'cats/browse/ألعاب-ورق-و-كتشينة';

$route['ألعاب-رياضة'] = 'cats/browse/ألعاب-رياضة';

$route['العاب-قتال'] = 'cats/browse/العاب-قتال';

$route['العاب-زنقة-2011'] = 'cats/browse/العاب-زنقة-2011';
/////

//$route['crossdomain.xml'] = 'inc/crossdomain.xml';



/// route the tages

 $route['tag/ألعاب-2012'] = 'tag/browse/2012';
 $route['tag/ألعاب-2012-للتحميل-'] = 'tag/browse/2012';
 $route['tag/ألعاب-أطفال'] = 'tag/browse/أطفال';
 $route['tag/ألعاب-2011'] = 'tag/browse/2011';
 $route['tag/ألعاب-أطفال-2012'] = 'tag/browse/2012';
 
  $route['tag/ألعاب-أونلاين'] = 'tag/browse/أونلاين';
 $route['tag/ألعاب-إثارة'] = 'tag/browse/إثارة';
  $route['tag/ألعاب-المغامرات'] = 'tag/browse/المغامرات';
 
  $route['tag/ألعاب-بنات'] = 'tag/browse/بنات';
  $route['tag/ألعاب-تركيز'] = 'tag/browse/تركيز';
  $route['tag/ألعاب-تصويب'] = 'tag/browse/تصويب';
  $route['tag/ألعاب-تفاعلية'] = 'tag/browse/تفاعلية';
  $route['tag/ألعاب-تفكير'] = 'tag/browse/تفكير';
  $route['tag/ألعاب-جديدة'] = 'tag/browse/جديدة';
  $route['tag/ألعاب-جديدة-حصرية'] = 'tag/browse/جديدة';
  $route['tag/ألعاب-جديده'] = 'tag/browse/جديده';
  $route['tag/ألعاب-حديثة'] = 'tag/browse/حديثة';
  $route['tag/ألعاب-حصرية'] = 'tag/browse/حصرية';
  $route['tag/ألعاب-حيوانات'] = 'tag/browse/حيوانات';
  $route['tag/ألعاب-دقة'] = 'tag/browse/دقة';
  $route['tag/ألعاب-ديكور-2012'] = 'tag/browse/ديكور';
  $route['tag/ألعاب-ذكاء'] = 'tag/browse/ذكاء';
  $route['tag/ألعاب-رائعة'] = 'tag/browse/رائعة';
  $route['tag/ألعاب-سرعة'] = 'tag/browse/سرعة';
  $route['tag/ألعاب-مهارات'] = 'tag/browse/مهارات';
  $route['tag/ألعاب-مضحكة'] = 'tag/browse/مضحكة';
  $route['tag/ألعاب-مسلية'] = 'tag/browse/مسلية';
  $route['tag/ألعاب-مثيرة'] = 'tag/browse/مثيرة';
  $route['tag/ألعاب-متنوعة'] = 'tag/browse/متنوعة';
  $route['tag/ألعاب-متقدمة'] = 'tag/browse/متقدمة';
  $route['tag/ألعاب-متطورة'] = 'tag/browse/متطورة';
  $route['tag/ألعاب-كلاسيكية'] = 'tag/browse/كلاسيكية';
  $route['tag/ألعاب-قدرات'] = 'tag/browse/قدرات';
  $route['tag/ألعاب-قتالية'] = 'tag/browse/قتالية';
  $route['tag/ألعاب-سريعة'] = 'tag/browse/سريعة';
$route['tag/ألعاب-قتال'] = 'tag/browse/قتال';
$route['tag/ألعاب-مميزة'] = 'tag/browse/مميزة';
$route['tag/ألعاب-مهارة'] = 'tag/browse/مهارة';
$route['tag/ألعاب-حرب'] = 'tag/browse/حرب';
$route['tag/ألعاب-مجانية'] = 'tag/browse/مجانية';
$route['tag/العاب-باربي'] = 'tag/browse/باربي';








/* End of file routes.php */
/* Location: ./application/config/routes.php */